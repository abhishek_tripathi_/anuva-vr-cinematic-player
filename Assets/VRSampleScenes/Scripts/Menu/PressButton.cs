﻿using UnityEngine;
using System;
using System.Collections;
using VRStandardAssets.Utils;


namespace VRStandardAssets.Menu
{
	// This script is for triggering event by pressing the Button.
	// Each 'button' will be a rendering optionally showing the event
	// that will be loaded and use the SelectionRadial.
	public class PressButton : MonoBehaviour 
	{
		public event Action<MenuButton> OnButtonSelected;                   // This event is triggered when the selection of the button has finished.

		public GameObject TVGrp01;
		public GameObject TVGrp02;

		[SerializeField] private VRCameraFade m_CameraFade;                 // This fades the scene out when a new scene is about to be loaded.
		[SerializeField] private SelectionRadial m_SelectionRadial;         // This controls when the selection is complete.
		[SerializeField] private VRInteractiveItem m_InteractiveItem;       // The interactive item for where the user should click to load the level.

		private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.


		public bool canSwitch = true;

		private static bool isShowingFirstModel;

		public void Awake()
		{
			UpdateAppliance ();

		}



		private void OnEnable ()
		{
			//curSelectionNo = 1;
			m_InteractiveItem.OnOver += HandleOver;
			m_InteractiveItem.OnOut += HandleOut;
			m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete;
		}

		private void HandleOver()
		{
			// When the user looks at the rendering of the scene, show the radial.
			m_SelectionRadial.Show();

			m_GazeOver = true;
		}


		private void HandleOut()
		{
			// When the user looks away from the rendering of the scene, hide the radial.
			m_SelectionRadial.Hide();

			m_GazeOver = false;
		}

		private void HandleSelectionComplete()
		{
			// If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
			if(m_GazeOver)
				 ActivateButton();
		}

		private void ActivateButton()
		{


			// If the camera is already fading, ignore.
//			if (m_CameraFade.IsFading)
//				yield break;

//			// If anything is subscribed to the OnButtonSelected event, call it.
//			if (OnButtonSelected != null)
//				OnButtonSelected(this);

//			// Wait for the camera to fade out.
//			yield return StartCoroutine(m_CameraFade.BeginFadeOut(true));

			//If the group is empty or have only one item, ignore

			canSwitch = true;


		}



		public void Update()
		{
			if (canSwitch)
			{
				UpdateAppliance ();

			}

		}



		public void UpdateAppliance ()
		{
			isShowingFirstModel = !isShowingFirstModel;
			TVGrp01.SetActive (isShowingFirstModel);
			TVGrp02.SetActive (!isShowingFirstModel);
			canSwitch = false;
		}



		}
}
