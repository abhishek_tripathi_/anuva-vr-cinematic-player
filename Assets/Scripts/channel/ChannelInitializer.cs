﻿using System.Collections;
using System.IO;
using Assets.Scripts.Channel_Umenu;
using Assets.Scripts.playlist;
using SimpleJSON;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.channel {
    public class ChannelInitializer : MonoBehaviour {

        public GameObject PlaylistCategoryObject;
        public GameObject VideoCategoryObject;
        public GameObject PlaylistObject;
        public GameObject VideoObject;
        public GameObject PlaylistInfoObject;
        public GameObject VideoInfoObject;
        public static bool IsBackPressed;
        public static bool Is360Video;
        public static bool IsBackPressedInDifferentScene;
        public static GameObject CurrentActivePlaylist;
        public float PlaylistInfoXPos;
        public static string VideoId;
        public GameObject NetworkText;
        //public GameObject YoutubeChannelsObject;
        public GameObject MoveObject;
        public bool _isVideoToBeDisplayed;

        //////// Specific to camera rotation....////////
        public GameObject Head;
        public CameraMovement CamMovement;
        public static float PlaylistMaxValue;
        public static float VideolistMaxValue;
        private int rot_x1_video =24;
        // private int rot_x2_video = 33;
        private int rot_x2_video = 50;
        //private int rot_x3_video = 50;

        private int rot_x4_video = 334;
        //private int rot_x5_video = 322;
        private int rot_x5_video = 301;
        //private int rot_x6_video = 301;

        private int rot_y1_video = 348; ////// up
        private int rot_y2_video = 10;   ////// 


        private int rot_x1_pList = 26;
        // private int rot_x2_video = 33;
        private int rot_x2_pList = 100;
        //private int rot_x3_pList = 50;

        private int rot_x4_pList = 330;
        //private int rot_x5_video = 322;
        private int rot_x5_pList = 310;
        //private int rot_x6_pList = 301;

        private int rot_y1_pList = 340; ////// up
        private int rot_y2_pList = 349;   ////// down


        void Start() {
            if (Application.internetReachability == NetworkReachability.NotReachable) {
                NetworkText.GetComponent<TextMeshProUGUI>().enabled = true;
                return;
            }
            
            StartCoroutine(PeriodicNetworkCheck());
            //StartCoroutine(Recenter());
            //if (IsBackPressedInDifferentScene) {
            //    IsBackPressedInDifferentScene = false;
            //    return;
            //}
           // StartCoroutine(PeriodicRecenter());
           GvrViewer.Instance.Recenter();
            IsBackPressed = false;
                PlaylistCategoryObject.SetActive(false);
            VideoCategoryObject.SetActive(false);
            foreach (Transform child in PlaylistObject.transform) {
                Destroy(child.gameObject);
            }
            foreach (Transform child in VideoObject.transform) {
                Destroy(child.gameObject);
            }

            var configFile = Resources.Load("Config") as TextAsset;
            var configStr = configFile.text;
            //if (string.IsNullOrEmpty(configStr)) {
            //    YoutubeChannelsObject.SetActive(true);
            //}
            //else {
            //    YoutubeChannelsObject.SetActive(false);
            //}
            //  PlaylistCategoryObject.SetActive(true);
            PlaylistInitializer.ChannelId = configStr;
           StartCoroutine(DisplayVideoOrPlaylist());
           
            //HomeChannelCategories.SetActive(false);
            ChannelInitializer.IsBackPressed = false;
            ChannelInitializer.Is360Video = true;
            GvrViewer.Instance.Recenter();
        }

        private void DisplayMyVideosPanel() {
            foreach (Transform child in VideoObject.transform) {
                Destroy(child.gameObject);
            }
            foreach (Transform child in PlaylistObject.transform) {
                Destroy(child.gameObject);
            }
            VideoCategoryObject.SetActive(false);
            PlaylistInfoInitializer.desc = "My Videos";
            PlaylistInfoInitializer.title = "My Videos";
            //PlaylistInfoInitializer.PlaylistId = PlaylistId;
            // PlaylistInfoInitializer.PlaylistImage = Sprite;
            PlaylistObject.SetActive(false);
            PlaylistInfoObject.SetActive(false);
            PlaylistCategoryObject.SetActive(false);
            PlaylistInfoInitializer.isFirst = true;
            VideoCategoryObject.SetActive(true);
            VideoInitializer.isMyVideosPlaylist = true;
            // VideoPanelParentObject.GetComponent<PlaylistInitializer>().OnEnable();
          //  VideoInitializer.PlaylistId = "UCE8ueIbAX-4VjXd9RQ6i4pQ";
            VideoInitializer.PlaylistId = PlaylistInitializer.ChannelId;
            ChannelInitializer.IsBackPressed = false;
        }

        private IEnumerator DisplayVideoOrPlaylist() {
              string YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
            var channelId = PlaylistInitializer.ChannelId;
           // var channelId = "UCE8ueIbAX-4VjXd9RQ6i4pQ";

            var call = new WWW("https://www.googleapis.com/youtube/v3/search?&key=" + YourAPIKey + "&part=snippet&channelId=" + channelId + "&type=video&maxResults=" + 50 + "");
            yield return call;
            var youtubeReturn = JSON.Parse(call.text);
            _isVideoToBeDisplayed =   (youtubeReturn["pageInfo"]["totalResults"].AsInt) == 0 ? false : true;
            if (false) {
              //  if (_isVideoToBeDisplayed) {
                DisplayMyVideosPanel();
            }
            else {
                VideoCategoryObject.SetActive(false);
                PlaylistCategoryObject.SetActive(true);
            }
        }

        private IEnumerator Recenter() {
            yield return new WaitForSeconds(2);
            GvrViewer.Instance.Recenter();
        }

        private IEnumerator PeriodicNetworkCheck() {
            while (true) {
                yield return new WaitForSeconds(4);
                NetworkText.GetComponent<TextMeshProUGUI>().enabled = (Application.internetReachability == NetworkReachability.NotReachable);
            }
        }

        IEnumerator PeriodicRecenter() {
            while (true) {
                yield return new WaitForSeconds(2);
                GvrViewer.Instance.Recenter();
            }
        }

        void OnEnable() {
            if (IsBackPressedInDifferentScene) {
                return;
            }
            MoveObject.transform.position = new Vector3(0, 0, MoveObject.transform.position.z);
            PlaylistInfoXPos = PlaylistInfoObject.GetComponent<RectTransform>().localPosition.x;
        }

        void Update() {
            if (PlaylistObject.activeInHierarchy || VideoObject.activeInHierarchy)
                UpdateRotation();

        }

        private void UpdateRotation() {
            if (CamMovement.IsMoving)
                return;

            var zeroBottomInBetweenConditionForVideo = Head.transform.localRotation.eulerAngles.x > 0 &&
                                              Head.transform.localRotation.eulerAngles.x < rot_y2_video;
            var zeroTopInBetweenConditionForVideo = Head.transform.localRotation.eulerAngles.x < 360 &&
                                              Head.transform.localRotation.eulerAngles.x > rot_y1_video;

            var zeroBottomInBetweenConditionForPList= Head.transform.localRotation.eulerAngles.x >= rot_y1_pList &&
                                             Head.transform.localRotation.eulerAngles.x <= rot_y2_pList;
            //var zeroTopInBetweenConditionForPList = Head.transform.localRotation.eulerAngles.x < 360 &&
            //                                  Head.transform.localRotation.eulerAngles.x > rot_y1_video;

            if (zeroTopInBetweenConditionForVideo || zeroBottomInBetweenConditionForVideo) {

                var rightFirstRegion = Head.transform.localRotation.eulerAngles.y > rot_x1_video &&
                                       Head.transform.localRotation.eulerAngles.y < rot_x2_video;
                
                var isMaxConditionReachedOnRight = false;
                //if (PlaylistCategoryObject.activeInHierarchy) {
                //    if (CamMovement.MoveObject.transform.position.x >= PlaylistMaxValue)
                //        isMaxConditionReachedOnRight = true;
                //} 
//                else 
                if (VideoCategoryObject.activeInHierarchy) {
                   // if (CamMovement.MoveObject.transform.position.x >= VideolistMaxValue)
                        if ((CamMovement.MoveObject.transform.position.x - VideoObject.transform.position.x) >= VideolistMaxValue)

                            isMaxConditionReachedOnRight = true;
                }

                if (rightFirstRegion && !isMaxConditionReachedOnRight) {
                    CamMovement.ArrowClicked = 1;
                    CamMovement.IsMoving = true;
                    CamMovement.moveSpeed = 0.8f;
                    CamMovement.isVideoListToBeMoved = true;
                    return;
                }

                //var rightSecondRegion = Head.transform.localRotation.eulerAngles.y > rot_x2_video &&
                //                       Head.transform.localRotation.eulerAngles.y < rot_x3_video;
                //if (rightSecondRegion) {
                //    CamMovement.ArrowClicked = 1;
                //    CamMovement.IsMoving = true;
                //    CamMovement.moveSpeed = 1f;
                //    return;
                //}

                var leftFirstRegion = Head.transform.localRotation.eulerAngles.y < rot_x4_video &&
                                       Head.transform.localRotation.eulerAngles.y > rot_x5_video;

                var isMaxConditionReachedOnLeft = false;
                //if (PlaylistCategoryObject.activeInHierarchy) {
                //    if (CamMovement.MoveObject.transform.position.x <= -CamMovement.videoPlaneDistance)
                //        isMaxConditionReachedOnLeft = true;
                //}
//                else 
                if (VideoCategoryObject.activeInHierarchy) {
                    if (VideoInitializer.isMyVideosPlaylist) {
                        if ((CamMovement.MoveObject.transform.position.x - VideoObject.transform.position.x) <= 0)
                            isMaxConditionReachedOnLeft = true;
                    }
                    else {
                        if ((CamMovement.MoveObject.transform.position.x - VideoObject.transform.position.x) <= -CamMovement.videoPlaneDistance)
                            isMaxConditionReachedOnLeft = true;
                    }
                   
                }
                if (leftFirstRegion && !isMaxConditionReachedOnLeft) {
                    CamMovement.ArrowClicked = -1;
                    CamMovement.IsMoving = true;
                    CamMovement.moveSpeed = 0.8f;
                    CamMovement.isVideoListToBeMoved = true;
                    return;
                }

                //var leftSecondRegion = Head.transform.localRotation.eulerAngles.y < rot_x5_video &&
                //                       Head.transform.localRotation.eulerAngles.y > rot_x6_video;
                //if (leftSecondRegion) {
                //    CamMovement.ArrowClicked = -1;
                //    CamMovement.IsMoving = true;
                //    CamMovement.moveSpeed = 1f;
                //}
            }else if(zeroBottomInBetweenConditionForPList) {
                var rightFirstRegion = Head.transform.localRotation.eulerAngles.y > rot_x1_pList &&
                                       Head.transform.localRotation.eulerAngles.y < rot_x2_pList;

                bool isMaxConditionReachedOnRight = (CamMovement.MoveObject.transform.position.x - PlaylistObject.transform.position.x) >= PlaylistMaxValue;

                if (rightFirstRegion && !isMaxConditionReachedOnRight) {
                    CamMovement.ArrowClicked = 1;
                    CamMovement.IsMoving = true;
                    CamMovement.moveSpeed = 0.8f;
                    CamMovement.isVideoListToBeMoved = false;
                    return;
                }

                //var rightSecondRegion = Head.transform.localRotation.eulerAngles.y > rot_x2_video &&
                //                       Head.transform.localRotation.eulerAngles.y < rot_x3_video;
                //if (rightSecondRegion) {
                //    CamMovement.ArrowClicked = 1;
                //    CamMovement.IsMoving = true;
                //    CamMovement.moveSpeed = 1f;
                //    return;
                //}

                var leftFirstRegion = Head.transform.localRotation.eulerAngles.y < rot_x4_pList &&
                                       Head.transform.localRotation.eulerAngles.y > rot_x5_pList;

                bool isMaxConditionReachedOnLeft = (CamMovement.MoveObject.transform.position.x - PlaylistObject.transform.position.x) <=0;

                if (leftFirstRegion && !isMaxConditionReachedOnLeft) {
                    CamMovement.ArrowClicked = -1;
                    CamMovement.IsMoving = true;
                    CamMovement.moveSpeed = 0.8f;
                    CamMovement.isVideoListToBeMoved = false;
                    return;
                }

                //var leftSecondRegion = Head.transform.localRotation.eulerAngles.y < rot_x5_video &&
                //                       Head.transform.localRotation.eulerAngles.y > rot_x6_video;
                //if (leftSecondRegion) {
                //    CamMovement.ArrowClicked = -1;
                //    CamMovement.IsMoving = true;
                //    CamMovement.moveSpeed = 1f;
                //}
            }
        }

        public Vector3 GetOriginalPosition(GameObject targetObj, float originalPosInX, bool isChild) {
            if (isChild)
                return new Vector3(originalPosInX, targetObj.GetComponent<RectTransform>().localPosition.y, targetObj.GetComponent<RectTransform>().localPosition.z);
            return new Vector3(originalPosInX, targetObj.transform.position.y, targetObj.transform.position.z);
        }
    }
}
