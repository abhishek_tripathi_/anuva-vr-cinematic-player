﻿using Assets.Scripts.Channel_Umenu;
using Assets.Scripts.playlist;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.channel {
    public class MyVideosSelector : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private Vector3 _originalScale;
        private Vector3 _hoverScale;
        [HideInInspector]
        public string TotalVideos;
        public GameObject[] ToggleVisibleObjects;
        public GameObject VideoPanelParentObject;
        public bool isToggleObjectsSelected;
        private SelectionRadial _selectionRadial;
        public bool _gazeOver;
        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public GameObject VideoGameObject;
        public GameObject PlaylistInfoParentObject;
        public GameObject PlaylistBG;
        public ChannelInitializer ChannelInit;

        void Awake() {
            VideoGameObject = GameObject.Find("Video");
            //VideoGameObject = VideoPanelParentObject.GetComponent<VideoInitializer>().VideoPanelParentObj;
            isToggleObjectsSelected = false;
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.8f, 0.8f, 0.8f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(true);
            }
            PlaylistBG.SetActive(true);
            //TitleObject.SetActive(true);
            //TitleObject.GetComponentInChildren<TextMeshProUGUI>().text = Title;

            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            //if (isToggleObjectsSelected)
            //    return;
            if ((ChannelInitializer.CurrentActivePlaylist == null) || !ChannelInitializer.CurrentActivePlaylist.Equals(gameObject))
                PlaylistBG.SetActive(false);
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(false);
            }
            //TitleObject.SetActive(false);
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;

            //if (ChannelInitializer.CurrentActivePlaylist != null) {
            //    ChannelInitializer.CurrentActivePlaylist.GetComponent<Animator>().Rebind();
            //    ChannelInitializer.CurrentActivePlaylist.GetComponent<Animator>().enabled = false;
            //    ChannelInitializer.CurrentActivePlaylist.GetComponent<CursorSelector>().PlaylistBG.SetActive(false);
            //    var currPos = ChannelInitializer.CurrentActivePlaylist.GetComponent<RectTransform>().localPosition;
            //    ChannelInitializer.CurrentActivePlaylist.GetComponent<RectTransform>().localPosition = new Vector3(currPos.x, currPos.y, 0);
            //}
            //ChannelInitializer.CurrentActivePlaylist = gameObject;
            HandleOut();
          //  gameObject.GetComponent<Animator>().enabled = true;
            //PlaylistBG.SetActive(true);

            //VideoPanelParentObject.SetActive(false);
            // VideoPanelParentObject.SetActive(true);
            foreach (Transform child in VideoGameObject.transform) {
                Destroy(child.gameObject);
            }
            VideoPanelParentObject.SetActive(false);
            ChannelInit.PlaylistObject.SetActive(false);
           PlaylistInfoParentObject.SetActive(false);
         //   PlaylistInfoInitializer.desc = "My Videos";
          //  PlaylistInfoInitializer.title = "My Videos";
            //PlaylistInfoInitializer.PlaylistId = PlaylistId;
           // PlaylistInfoInitializer.PlaylistImage = Sprite;
           ChannelInit.PlaylistCategoryObject.SetActive(false);
          //  PlaylistInfoInitializer.isFirst = true;
            VideoPanelParentObject.SetActive(true);
            VideoInitializer.isMyVideosPlaylist = true;
            // VideoPanelParentObject.GetComponent<PlaylistInitializer>().OnEnable();
            //VideoInitializer.PlaylistId = "UCE8ueIbAX-4VjXd9RQ6i4pQ";
            VideoInitializer.PlaylistId = PlaylistInitializer.ChannelId;
            ChannelInitializer.IsBackPressed = false;

            //  PlaylistInfoParentObject.SetActive(false);
            //  PlaylistInfoParentObject.SetActive(true);


           ChannelInit.MoveObject.transform.position = new Vector3(0, 0, ChannelInit.MoveObject.transform.position.z);
            ChannelInit.VideoObject.transform.position = ChannelInit.GetOriginalPosition(ChannelInit.VideoObject, 0, false);
            ChannelInit.PlaylistObject.transform.position = ChannelInit.GetOriginalPosition(ChannelInit.PlaylistObject, 0, false);
            ChannelInit.PlaylistInfoObject.GetComponent<RectTransform>().localPosition = ChannelInit.GetOriginalPosition(ChannelInit.PlaylistInfoObject, ChannelInit.PlaylistInfoXPos, true);

        }
    }
}
