﻿using UnityEngine;

namespace Assets.Scripts.channel {
    public class ColliderBox : MonoBehaviour {

        private GameObject _currentActiveParentPrefab;
        private Vector3 _originalPosition;

        void OnTriggerEnter(Collider other) {
            if (_currentActiveParentPrefab != null) {
                _currentActiveParentPrefab.GetComponent<RectTransform>().localPosition = _originalPosition;
                _currentActiveParentPrefab.GetComponent<Animator>().Rebind();
                _currentActiveParentPrefab.GetComponent<Animator>().enabled = false;
            }
            var colliderParentPrefab = other.gameObject;
            _originalPosition = colliderParentPrefab.GetComponent<RectTransform>().localPosition;
            _currentActiveParentPrefab = colliderParentPrefab;
            colliderParentPrefab.GetComponent<Animator>().enabled = true;

        }
    }
}
