﻿using Assets.Scripts.video;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

namespace Assets.Scripts.Vr {
    public class SliderHandler : MonoBehaviour {

        public VRInteractiveItem VrItem;
        public float SliderPercentageValue;
        public Slider Slider;
        private SelectionRadial _selectionRadial;
        private bool _isGazeOver;
        public MediaPlayerCtrl MediaCtrl;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _isGazeOver = false;
        }

        void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            if (Initializer._isGazeOver)
                return;

            _isGazeOver = true;
            Initializer._isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
        }

        private void HandleOut() {
            if (!Initializer._isGazeOver)
                return;

            _isGazeOver = false;
            Initializer._isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;
            var maxSliderValue = Slider.maxValue;
            var newSliderValue = CalculateSliderValue(maxSliderValue);
            Slider.value = newSliderValue;
            MediaCtrl.SeekTo((int)Slider.value * 1000);
            HandleOut();
        }

        private float CalculateSliderValue(float maxSliderValue) {
            return (SliderPercentageValue * maxSliderValue) / 100f;
        }
    }
}
