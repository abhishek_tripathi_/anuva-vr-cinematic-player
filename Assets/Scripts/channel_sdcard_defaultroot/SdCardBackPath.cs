﻿using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.channel_sdcard_defaultroot {
    public class SdCardBackPath : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        private bool _gazeOver;
        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public SdCardDefaultRootVideoInitializer SdCardVideoInit;

        private void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.6f, 0.6f, 0.6f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;
            HandleOut();

            var currAbsolutePath = SdCardDefaultRootInitializer.CurrentAbsolutePath;
            var lastIndex = currAbsolutePath.LastIndexOf("/");
            SdCardDefaultRootInitializer.CurrentAbsolutePath = currAbsolutePath.Substring(0, lastIndex);
            var channelInit = SdCardVideoInit.ChannelInit;
            channelInit.VideoObject.transform.position = new Vector3(0, channelInit.VideoObject.transform.position.y, channelInit.VideoObject.transform.position.z);
            channelInit.MoveObject.transform.position = new Vector3(0, 0, channelInit.MoveObject.transform.position.z);
            SdCardVideoInit.InitLoad();
        }

    }
}
