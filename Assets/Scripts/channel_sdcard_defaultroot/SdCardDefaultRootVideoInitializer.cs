﻿using System.Collections;
using Assets.Scripts.channel_sdcard.ChannelHome;
using UnityEngine;

namespace Assets.Scripts.channel_sdcard_defaultroot {
    public class SdCardDefaultRootVideoInitializer : MonoBehaviour {

        private Vector3[] _prefabPos = {

           new Vector3(-436, 305, -0),               ///// Image 1
            new Vector3(-70,305, -0),                         ///// Image 2
            new Vector3(295,305, -0),
            new Vector3(664,305, -0),           ///// Image 3

            new Vector3(-436, 72, -0),               ///// Image 1
            new Vector3(-70,72, -0),                         ///// Image 2
            new Vector3(295,72, -0),
            new Vector3(664,72, -0),           ///// Image 3


            new Vector3(-436, -162, -0),               ///// Image 1
            new Vector3(-70,-162, -0),                         ///// Image 2
            new Vector3(295,-162, -0),
            new Vector3(664,-162, -0),           ///// Image 3


           new Vector3(-436, -397, -0),               ///// Image 1
            new Vector3(-70,-397, -0),                         ///// Image 2
            new Vector3(295,-397, -0),
            new Vector3(664,-397, -0),

        };

        private Vector3[] _prefabRot = {

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),
        };
        private int _gridCount = 16;
        private GameObject _parentObj = null;
        private bool _isAllLoaded;
        public GameObject LoadingIcon;
        public GameObject LoadingText;

        public GameObject Prefab;
        public GameObject ParentPrefab;
        static private int currentCount = 0;
        private int _parentCount;
        private float cameraDistanceToMove = 46.9f;
        public GameObject VideoPanelParentObj;
        public SdCardDefaultRootInitializer ChannelInit;
        public GameObject BackPathObject;

        void OnEnable() {
            InitLoad();
        }

        public void InitLoad() {
            currentCount = 0;
            _parentCount = 1;
            _isAllLoaded = false;
            DestroyAllVideoChild();
            VideoPanelParentObj.SetActive(false);
            StartCoroutine(ShowLoadingIcon());
            StartCoroutine(LoadAllFiles());
        }

        private IEnumerator ShowLoadingIcon() {
            LoadingIcon.SetActive(true);
            LoadingText.SetActive(true);
            while (!_isAllLoaded) {
                yield return null;
            }
            LoadingIcon.SetActive(false);
            LoadingText.SetActive(false);
        }

        void Update() {
            if (_isAllLoaded) {
                SdCardDefaultRootInitializer.VideolistMaxValue = (_parentCount - 2) * cameraDistanceToMove;
            }
            BackPathObject.SetActive(!(SdCardDefaultRootInitializer.CurrentAbsolutePath.Equals(SdCardDefaultRootInitializer.RootAbsolutePath)));
        }

        private void DestroyAllVideoChild() {
            foreach (Transform child in VideoPanelParentObj.transform) {
                Destroy(child.gameObject);
            }
        }

        private IEnumerator LoadAllFiles() {
            var fileObject = new AndroidJavaObject("java.io.File", SdCardDefaultRootInitializer.CurrentAbsolutePath);
            var filesList = fileObject.Call<string[]>("list");
            yield return null;

            foreach (string fileName in filesList) {
                var file = new AndroidJavaObject("java.io.File", SdCardDefaultRootInitializer.CurrentAbsolutePath + "/" + fileName);
                var isDirectory = file.Call<bool>("isDirectory");
                if (!isDirectory)
                    continue;

                if (((currentCount + 1) % _gridCount) == 1)
                    _parentObj = InstantiateParentPrefab();
                var fileObj = Instantiate(Prefab);
                fileObj.transform.parent = _parentObj.transform;
                var filePos = _prefabPos[(currentCount % _gridCount)];
                fileObj.GetComponent<RectTransform>().transform.localPosition = filePos;
                fileObj.GetComponent<RectTransform>().transform.localScale = new Vector3(1, 1, 1);
                fileObj.GetComponent<SdCardFileSelector>().FilePath = fileName;
                // ChannelInit.InfoTextObject.GetComponent<TextMeshProUGUI>().text += fileName + "\n";
                currentCount++;
                yield return null;
            }

            VideoPanelParentObj.SetActive(true);
            _isAllLoaded = true;
        }

        private GameObject InstantiateParentPrefab() {
            var parentObject = Instantiate(ParentPrefab);
            parentObject.transform.position = Vector3.zero;
            PlacePrefabInWorld(parentObject);
            return parentObject;
        }

        private void PlacePrefabInWorld(GameObject parentObject) {
            var posX = 0f;
            posX = (_parentCount - 1) * cameraDistanceToMove;
            parentObject.transform.parent = VideoPanelParentObj.transform;
            parentObject.GetComponent<RectTransform>().transform.localPosition = new Vector3(posX, 0, 16.1f);
            _parentCount++;
        }
    }
}
