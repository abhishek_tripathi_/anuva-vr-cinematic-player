﻿using System.Collections;
using Assets.Scripts.channel_sdcard.ChannelHome;
using UnityEngine;

namespace Assets.Scripts.channel_sdcard_defaultroot {
    public class SdCardDefaultRootInitializer : MonoBehaviour {

        public GameObject VideoCategoryObject;
        public GameObject VideoObject;
        public static string VideoId;
        //public GameObject YoutubeChannelsObject;
        public GameObject MoveObject;
        public static string RootAbsolutePath;
        public static string CurrentAbsolutePath;
        public GameObject InfoTextObject;

        //////// Specific to camera rotation....////////
        public GameObject Head;
        public SdCardCameraMovement CamMovement;
        public static float VideolistMaxValue;

        private int rot_x1_video = 24;
        // private int rot_x2_video = 33;
        private int rot_x2_video = 50;
        //private int rot_x3_video = 50;

        private int rot_x4_video = 334;
        //private int rot_x5_video = 322;
        private int rot_x5_video = 301;
        //private int rot_x6_video = 301;

        private int rot_y1_video = 348; ////// up
        private int rot_y2_video = 10;   ////// 

        void Start() {
           
            // StartCoroutine(PeriodicRecenter());
            GvrViewer.Instance.Recenter();
            VideoCategoryObject.SetActive(false);
            foreach (Transform child in VideoObject.transform) {
                Destroy(child.gameObject);
            }
            SetRootAbsolutePath();
            SetCurrentAbsolutePath();
            VideoCategoryObject.SetActive(true);
            GvrViewer.Instance.Recenter();
            // InfoTextObject.GetComponent<TextMeshProUGUI>().text += RootAbsolutePath + "\n";
        }

        private static void SetRootAbsolutePath() {
            var jo = new AndroidJavaClass("android.os.Environment");
            var rootDir = jo.CallStatic<AndroidJavaObject>("getExternalStorageDirectory");
            RootAbsolutePath = rootDir.Call<string>("getAbsolutePath");

        }

        private static void SetCurrentAbsolutePath() {
            CurrentAbsolutePath = RootAbsolutePath;
        }

        private IEnumerator Recenter() {
            yield return new WaitForSeconds(2);
            GvrViewer.Instance.Recenter();
        }

        IEnumerator PeriodicRecenter() {
            while (true) {
                yield return new WaitForSeconds(2);
                GvrViewer.Instance.Recenter();
            }
        }

        void OnEnable() {
            MoveObject.transform.position = new Vector3(0, 0, MoveObject.transform.position.z);
        }

        void Update() {
            if (VideoObject.activeInHierarchy)
                UpdateRotation();
        }

        private void UpdateRotation() {
            if (CamMovement.IsMoving)
                return;

            var zeroBottomInBetweenConditionForVideo = Head.transform.localRotation.eulerAngles.x > 0 &&
                                              Head.transform.localRotation.eulerAngles.x < rot_y2_video;
            var zeroTopInBetweenConditionForVideo = Head.transform.localRotation.eulerAngles.x < 360 &&
                                              Head.transform.localRotation.eulerAngles.x > rot_y1_video;

            if (zeroTopInBetweenConditionForVideo || zeroBottomInBetweenConditionForVideo) {

                var rightFirstRegion = Head.transform.localRotation.eulerAngles.y > rot_x1_video &&
                                       Head.transform.localRotation.eulerAngles.y < rot_x2_video;

                var isMaxConditionReachedOnRight = false;
                if (VideoCategoryObject.activeInHierarchy) {
                    if ((CamMovement.MoveObject.transform.position.x - VideoObject.transform.position.x) >= VideolistMaxValue)

                        isMaxConditionReachedOnRight = true;
                }

                if (rightFirstRegion && !isMaxConditionReachedOnRight) {
                    CamMovement.ArrowClicked = 1;
                    CamMovement.IsMoving = true;
                    CamMovement.moveSpeed = 0.8f;
                    return;
                }

                var leftFirstRegion = Head.transform.localRotation.eulerAngles.y < rot_x4_video &&
                                       Head.transform.localRotation.eulerAngles.y > rot_x5_video;

                var isMaxConditionReachedOnLeft = false;
                if (VideoCategoryObject.activeInHierarchy) {
                    if ((CamMovement.MoveObject.transform.position.x - VideoObject.transform.position.x) <= 0)
                        isMaxConditionReachedOnLeft = true;
                }
                if (leftFirstRegion && !isMaxConditionReachedOnLeft) {
                    CamMovement.ArrowClicked = -1;
                    CamMovement.IsMoving = true;
                    CamMovement.moveSpeed = 0.8f;
                    return;
                }

            }
        }
    }
}
