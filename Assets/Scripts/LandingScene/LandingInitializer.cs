﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.LandingScene {
    public class LandingInitializer : MonoBehaviour {

        public Sprite[] SpriteImages;
        public GameObject ImageHolder;
        private int _currentImageIndex;
        public GameObject NextIcon;
        public GameObject StartIcon;

        void Awake() {
            //StartCoroutine(Recenter());
            GvrViewer.Instance.Recenter();
            _currentImageIndex = 0;
            ImageHolder.GetComponent<Image>().sprite = SpriteImages[_currentImageIndex];
        }

        private IEnumerator Recenter() {
            yield return new WaitForSeconds(2);
            GvrViewer.Instance.Recenter();
        }

        public void OnNextClick() {
            _currentImageIndex++;
            ImageHolder.GetComponent<Image>().sprite = SpriteImages[_currentImageIndex];
            if (_currentImageIndex >= SpriteImages.Length - 1) {
                StartIcon.SetActive(true);
                NextIcon.SetActive(false);
                
            }
        }

        public void OnSkipClick() {
            SceneManager.LoadScene(Constants.HomeScene);
        }

        public void OnStartClick() {
            SceneManager.LoadScene(Constants.HomeScene);
        }

    }
}
