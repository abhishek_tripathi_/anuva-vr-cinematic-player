﻿using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.LandingScene {
    public class StartImage : MonoBehaviour {
        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        public LandingInitializer LandingInit;
        private bool _isGazeOver;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _isGazeOver = false;
        }

        void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            gameObject.GetComponents<AudioSource>()[0].Play();
            _isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
        }

        private void HandleOut() {
            _isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;

            gameObject.GetComponents<AudioSource>()[1].Play();
            HandleOut();
            LandingInit.OnStartClick();
        }

    }
}
