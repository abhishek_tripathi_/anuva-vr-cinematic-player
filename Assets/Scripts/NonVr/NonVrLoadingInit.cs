﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.NonVr {
    public class NonVrLoadingInit : MonoBehaviour {

        void Start() {
            StartCoroutine(LoadNewScene());
        }

        IEnumerator LoadNewScene() {

            // This line waits for 3 seconds before executing the next line in the coroutine.
            // This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
          

            // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
            AsyncOperation async = SceneManager.LoadSceneAsync(Constants.NonVrScene);
            async.allowSceneActivation = false;
            // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
            while (async.isDone) {
                yield return null;
            }

            async.allowSceneActivation = true;

        }
    }
}
