﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.NonVr {
    public class ResolutionHandler : MonoBehaviour {

        public GameObject InitScript;
        public GameObject CurrentResolution;
        [HideInInspector]
        public GameObject NewResolution;

        void OnEnable() {
            CurrentResolution.transform.GetChild(0).gameObject.SetActive(true);
            InitScript.GetComponent<IInitializer>().SetResolution(CurrentResolution.GetComponent<ChangeResolution>().ObjectResolution);

          //  InitScript.ChangeResolution(CurrentResolution.GetComponent<ChangeResolution>().ObjectResolution);
        }

        public void ChangeCurrentResolution() {
            if (CurrentResolution.Equals(NewResolution))
                return; ///////// do nothing

            CurrentResolution.transform.GetChild(0).gameObject.SetActive(false);
            CurrentResolution = NewResolution;
            CurrentResolution.transform.GetChild(0).gameObject.SetActive(true);
            InitScript.GetComponent<IInitializer>().ChangeResolution(CurrentResolution.GetComponent<ChangeResolution>().ObjectResolution);
        }
    }
}
