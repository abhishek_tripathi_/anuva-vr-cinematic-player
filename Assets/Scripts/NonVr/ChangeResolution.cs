﻿using System;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.NonVr {
    public class ChangeResolution : MonoBehaviour {


        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        private bool _isGazeOver;
        public int ObjectResolution;
        public ResolutionHandler ResolutionHandler;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _isGazeOver = false;
        }

        void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            _isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
        }

        private void HandleOut() {
            _isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;
            HandleOut();
            ResolutionHandler.NewResolution = gameObject;
           ResolutionHandler.ChangeCurrentResolution();
        }


    }
}
