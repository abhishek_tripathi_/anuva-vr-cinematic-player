﻿using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.NonVr {
    public class ZoomInScreen : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        public NonVrInit NonVrInit;
        public GameObject ScreenObject;
        private Vector3 _currentTransform;
        private Vector3 _targetTransform;
        private bool _isGazeOver;
        private bool _isMoving;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _currentTransform = _targetTransform = ScreenObject.transform.position;
            _isGazeOver = false;
            _isMoving = false;
        }

        void Update() {
          if(!_isMoving)
                return;

            UpdateScreenPosition();
           Debug.Log(_isMoving);
        }

        private void UpdateScreenPosition() {
            ScreenObject.transform.position = Vector3.MoveTowards(_currentTransform, _targetTransform, 0.5f);
            _currentTransform = ScreenObject.transform.position;
            if (_currentTransform.Equals(_targetTransform)) {
                _isMoving = false;
                NonVrInit.CurrentScreenPosIndex++;
            }
               
        }

        void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            _isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
        }
        
        private void HandleOut() {
            _isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;

            var posInX = ScreenObject.transform.position.x;
            var posInY = ScreenObject.transform.position.y;
           
            var posInZ = NonVrInit._screenPosInZ[NonVrInit.CurrentScreenPosIndex+1];
            _currentTransform = ScreenObject.transform.position;
            _targetTransform = new Vector3(posInX,posInY,posInZ);
            HandleOut();
            _isMoving = true;
        }


    }
}
