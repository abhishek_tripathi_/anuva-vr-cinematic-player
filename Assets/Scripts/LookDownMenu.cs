﻿using UnityEngine;

namespace Assets.Scripts {
    public class LookDownMenu : MonoBehaviour {

        public GameObject TargetObject;
      //  public GameObject HidingPlane;
        public GameObject[] HidingObjects;

        ////// params
        public int FirstAngle;
        public int SecondAngle;

        void Update() {
            UpdateRotation();
        }

        private void UpdateRotation() {
            if (TargetObject.transform.localRotation.eulerAngles.x < FirstAngle || TargetObject.transform.localRotation.eulerAngles.x > SecondAngle) {
                gameObject.transform.localRotation = Quaternion.Euler(gameObject.transform.localRotation.eulerAngles.x,
                    TargetObject.transform.localRotation.eulerAngles.y, 0);
              //  HidingPlane.SetActive(true);
                foreach (var hidingobj in HidingObjects) {
                    if (hidingobj.GetComponent<MeshRenderer>() == null)
                        hidingobj.GetComponent<Canvas>().enabled = false;
                    else
                    hidingobj.GetComponent<MeshRenderer>().enabled = false;
                }
            }
            else {
                //Color color = HidingPlane.GetComponent<MeshRenderer>().material.color;
                //color.a += 0.1f;
                //HidingPlane.GetComponent<MeshRenderer>().material.color = new Color(color.r,color.g,color.b,color.a);
                //   HidingPlane.SetActive(false);
                foreach (var hidingobj in HidingObjects) {
                    if (hidingobj.GetComponent<MeshRenderer>() == null)
                        hidingobj.GetComponent<Canvas>().enabled = true;
                    else
                        hidingobj.GetComponent<MeshRenderer>().enabled = true;
                }
            }

        }
    }
}
