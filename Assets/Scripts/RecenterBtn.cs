﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts {
    public class RecenterBtn : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        public GameObject ReticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        private bool _isGazeOver;
        public GameObject RecenterInfoText;
        private int timeToWaitForRecenter;
       // private int timeRemaining;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(1, 1, 1);
            _reticleTransform = ReticleGameObject.GetComponent<RectTransform>();
            _isGazeOver = false;
            timeToWaitForRecenter = 3;
         //   timeRemaining = timeToWaitForRecenter;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionCompleteHome += HandleSelectionComplete;

            //_vrCameraFade.OnFadeCompleteHome += HandleFadeCompleteHome;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionCompleteHome -= HandleSelectionComplete;
            // _vrCameraFade.OnFadeCompleteHome -= HandleFadeCompleteHome;
            //  _vrCameraFade.OnFadeComplete -= HandleFadeComplete;
        }

        private void HandleOver() {
            _isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
            _selectionRadial.isHomeClicked = true;
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _selectionRadial.isHomeClicked = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

       
        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;
            HandleOut();
            StartCoroutine(Recenter());
        }

        private IEnumerator Recenter() {
           // yield return new WaitForSeconds(timeToWaitForRecenter);
            //GvrViewer.Instance.Recenter();

            while (true) {
                if (RecenterInfoText != null) {
                    RecenterInfoText.GetComponent<TextMeshProUGUI>().text = "Re-centering the view in " + timeToWaitForRecenter + " secs";
                }
                yield return new WaitForSeconds(1);
                timeToWaitForRecenter -= 1;
               
                if (timeToWaitForRecenter <= 0) {
                    GvrViewer.Instance.Recenter();
                    if(RecenterInfoText != null)
                        RecenterInfoText.GetComponent<TextMeshProUGUI>().text = string.Empty;
                    timeToWaitForRecenter = 3;
                    break;
                }
            }
        }
    }
}
