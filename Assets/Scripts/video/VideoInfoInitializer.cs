﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Assets.Scripts.channel;
using SimpleJSON;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.video {
    public class VideoInfoInitializer : MonoBehaviour {

        public static string desc;
        public static string title;
        public static string duration;
        public static string views;
        public static string likes;
        public static string dislikes;
        public static string VideoId;
        public static bool isFirst;
        public string channelName;
        public static int totalChunks;
        public static int currentChunk;

        public GameObject DescObject;
        public GameObject TitleGameObject;
        public GameObject DurationObject;
        public GameObject ViewObject;
        public GameObject LikeObject;
        public GameObject DislikeObject;
        public GameObject OwnerObject;
        public static Sprite VideoImage;
        public GameObject ImageObject;
        public GameObject MoveObject;
        public GameObject _360Icon;

        public GameObject PrevBtn;
        public GameObject NextBtn;

        private bool isHighResImageLoaded;

        private int _descOverflowLimit = 504;
        public List<string> descChunks;


        void Start() {

        }

        void OnEnable() {
            MoveObject.transform.position = new Vector3(0, 0, MoveObject.transform.position.z);
            ImageObject.GetComponent<Image>().sprite = null;
            channelName = null;
            isHighResImageLoaded = false;
            _descOverflowLimit = 504;
            _360Icon.SetActive(ChannelInitializer.Is360Video);
           
        }

        void Update() {
            if (!isFirst) {
                NextBtn.SetActive(currentChunk < totalChunks);
                PrevBtn.SetActive(currentChunk > 1);
                return;
            }

            StartCoroutine(GetChannelId());
            if (!string.IsNullOrEmpty(desc)) {
                desc = Regex.Replace(desc, @"[^\u0000-\u007F]", string.Empty);
                totalChunks = (int) Mathf.Ceil((float) desc.Length/(float) _descOverflowLimit);
                descChunks = new List<string>(totalChunks);
                var next = 0;
                for (var i = 0; i < totalChunks; i++) {
                    if (i == totalChunks - 1) {
                        descChunks.Add(desc.Substring(next,desc.Length-next));
                        break;
                    }
                    descChunks.Add(desc.Substring(next, _descOverflowLimit - 1));
                    //descChunks.Add(desc.Substring(next, _descOverflowLimit - 1));

                    next = next + _descOverflowLimit;
                }
               
            }
            DescObject.GetComponent<TextMeshProUGUI>().text = (descChunks == null || descChunks.Count ==0) ? string.Empty : descChunks[0];
            // TitleGameObject.GetComponent<TextMeshProUGUI>().text = (title.Length > 50 ) ? title.Substring(0,50) + "..."  : title;
            title = Regex.Replace(title, @"[^\u0000-\u007F]", string.Empty);
            TitleGameObject.GetComponent<TextMeshProUGUI>().text = title;
            DurationObject.GetComponent<TextMeshProUGUI>().text = duration;
            //ViewObject.GetComponent<TextMeshProUGUI>().text = (Convert.ToInt32(views) > 999999) ? "1M+" : views;
            
            ViewObject.GetComponent<TextMeshProUGUI>().text = String.Format("{0:n0}", Convert.ToInt32(views)); ;
            //LikeObject.GetComponent<TextMeshProUGUI>().text = (Convert.ToInt32(views) > 999999) ? "1M+" : likes;
            LikeObject.GetComponent<TextMeshProUGUI>().text = String.Format("{0:n0}", Convert.ToInt32(likes));
            //DislikeObject.GetComponent<TextMeshProUGUI>().text = (Convert.ToInt32(views) > 999999) ? "1M+" : dislikes;
            DislikeObject.GetComponent<TextMeshProUGUI>().text = String.Format("{0:n0}", Convert.ToInt32(dislikes));

            ImageObject.GetComponent<Image>().sprite = VideoImage;
            OwnerObject.GetComponent<TextMeshProUGUI>().text = channelName;
            currentChunk = 1;
            isFirst = false;
        }

        IEnumerator GetChannelId() {
            var YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
            var ncal = new WWW("https://www.googleapis.com/youtube/v3/videos?key=" + YourAPIKey + "&part=snippet&id=" + VideoId);
            yield return ncal;
            var youtubeNewReturn = JSON.Parse(ncal.text);
            youtubeNewReturn = youtubeNewReturn["items"];
           // StartCoroutine(DownloadThumbs(youtubeNewReturn[0]["snippet"]["thumbnails"]["standard"]["url"]));
            var channelId = youtubeNewReturn[0]["snippet"]["channelId"];
            if (string.IsNullOrEmpty(channelId)) {
                OwnerObject.GetComponent<TextMeshProUGUI>().text = channelName = "ANUVA VR";
                yield return null;
            }
            StartCoroutine(GetChannelInfo(channelId));
        }

        IEnumerator GetChannelInfo(string channelId) {
            var YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
            var ncal = new WWW("https://www.googleapis.com/youtube/v3/channels?key=" + YourAPIKey + "&part=snippet&id=" + channelId);
            yield return ncal;
            var youtubeNewReturn = JSON.Parse(ncal.text);
            youtubeNewReturn = youtubeNewReturn["items"];
            if (youtubeNewReturn == null)
                yield return null;
            OwnerObject.GetComponent<TextMeshProUGUI>().text = channelName = youtubeNewReturn[0]["snippet"]["title"];
            // var channelId = youtubeNewReturn[0]["snippet"]["channelId"];
        }

        private IEnumerator DownloadThumbs(string url) {
            var www = new WWW(url);
            yield return www;
            var texture = new Texture2D(100, 100);
            www.LoadImageIntoTexture(texture);
            //var rec = new Rect(0, 0, texture.width, texture.height);
            var rec = new Rect(0, 0, texture.height, texture.height);
            ImageObject.GetComponent<Image>().sprite = Sprite.Create(texture, rec, new Vector2(0, 0), 1);
           // isHighResImageLoaded = true;
        }
    }
}
