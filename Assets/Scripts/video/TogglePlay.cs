﻿using System.Net;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.video {
    public class TogglePlay : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        public Material PauseMaterial;
        public Material PlayMaterial;
        public MediaPlayerCtrl MediaCtrl;
        private bool _isPlaying;
        private bool _isGazeOver;
        public GameObject ReticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _isPlaying = true;
            _isGazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(1, 1, 1);
            _reticleTransform = ReticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;

        }

        private void HandleOver() {
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
            _isGazeOver = true;
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _isGazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;
            if (_isPlaying)
                ToggleToPause();
            else
                ToggleToPlay();
            _isGazeOver = false;
        }

        private void ToggleToPause() {
            _isPlaying = false;
            MediaCtrl.Pause();
            gameObject.GetComponent<MeshRenderer>().material = PlayMaterial;
        }

        private void ToggleToPlay() {
            _isPlaying = true;
            MediaCtrl.Play();
            gameObject.GetComponent<MeshRenderer>().material = PauseMaterial;
        }

    }
}
