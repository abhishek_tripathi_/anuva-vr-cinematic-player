﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

namespace Assets.Scripts.video {
    public class VideoSelector : MonoBehaviour {

        public VRInteractiveItem VrItem;
        //  private SelectionRadial _selectionRadial;
        // private VRCameraFade _vrCameraFade;
        //    [HideInInspector]public int AssetIndex;
        //    private bool _gazeOver;
        private Vector3 _originalScale;
        private Vector3 _hoverScale;
        //  private Vector3 _reticleOrigSize;
        //   private Vector3 _reticleOverSize;
        //   private Transform _reticleTransform;
        //[HideInInspector]
        public GameObject DurationObj;
        public GameObject TitleGameObject;
        public string VideoId;
        [HideInInspector]
        public string Desc;
        [HideInInspector]
        public string Title;
        [HideInInspector]
        public string OrigTitle;
        [HideInInspector]
        public string Duration;
        [HideInInspector]
        public string Likes;
        [HideInInspector]
        public string Dislikes;
        [HideInInspector]
        public string Views;
        [HideInInspector]
        public bool Is360Video;
        [HideInInspector] public Sprite Image;
        public GameObject[] ToggleVisibleObjects;
        public bool isToggleObjectsSelected;

        void Start() {
            isToggleObjectsSelected = false;
            DurationObj.GetComponent<TextMeshProUGUI>().text = Duration;
            TitleGameObject.GetComponent<TextMeshProUGUI>().text = Title;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
        }

        private void HandleOver() {
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(true);
            }
        }

        private void HandleOut() {
            if (isToggleObjectsSelected)
                return;
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(false);
            }
        }

    }
}
