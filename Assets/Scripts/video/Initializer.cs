﻿using System;
using System.Collections;
using Assets.Scripts.channel;
using Assets.Scripts.channel_sdcard.ChannelHome;
using Assets.Scripts.home;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VRStandardAssets.Utils;

namespace Assets.Scripts.video {
    public class Initializer : MonoBehaviour,IInitializer {

        public MediaPlayerCtrl MediaPlayerCtrl;
        public Slider Slider;
        public GameObject Errortext;
        private VRCameraFade _vrCameraFade;
        public static bool _isGazeOver;
        public GameObject LoadingIcon;
        public GameObject LoadingText;
        private int _currentResolution;
        private bool _isResChanged;
        private float _currentSliderValue;
        public GameObject StartTime;
        public GameObject EndTime;

        void Awake() {
            StartCoroutine(Recenter());
            SceneManager.UnloadScene(Constants.ChannelScene);
            _isResChanged = false;
            LoadingIcon.SetActive(true);
            LoadingText.SetActive(true);
            _isGazeOver =false;
            MediaPlayerCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName = string.Empty;
        }

        private IEnumerator Recenter() {
            yield return new WaitForSeconds(2);
            GvrViewer.Instance.Recenter();
        }


        private string GetVideoFileName() {
            if (HomeInitializer.ChannelClicked.Equals(Constants.SdCardChannelClicked))
                return GetSdCardVideoFileName();

            return GetYoutubeVideoFileName();

        }

        private string GetSdCardVideoFileName() {
          //  LoadingText.SetActive(true);
            //LoadingText.GetComponent<TextMeshProUGUI>().text = "file:///" + SdCardChannelInitializer.VideoId;
            return "file:///" + SdCardChannelInitializer.VideoId;
        }

        private string GetYoutubeVideoFileName() {
            if (Application.internetReachability == NetworkReachability.NotReachable) {
                return null;
            }
            var youtubeVideo = new YoutubeVideo();
            var url = youtubeVideo.RequestVideo(ChannelInitializer.VideoId, _currentResolution);
            return url;
        }

        IEnumerator Start() {
            StartCoroutine(ShowLoadingIcon());
            GvrViewer.Instance.Recenter();
            _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            yield return new WaitForSeconds(1);
            var videoFile = GetVideoFileName();
            MediaPlayerCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName = videoFile;
            MediaPlayerCtrl.Play();
        }

        private IEnumerator ShowLoadingIcon() {
            while (string.IsNullOrEmpty(MediaPlayerCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName)) {
                yield return null;
            }
            LoadingIcon.SetActive(false);
            LoadingText.SetActive(false);
        }


        void Update() {
            if (string.IsNullOrEmpty(MediaPlayerCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName))
                return;
            //if (Math.Abs(Slider.value - Slider.maxValue) < 0.001) {
            //    StartCoroutine(FadeToMenu());
            //    return;
            //}

            if (MediaPlayerCtrl.GetComponent<MediaPlayerCtrl>().GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.END) {
                StartCoroutine(FadeToMenu());
                return;
            }

            if (MediaPlayerCtrl.GetDuration() != 0) {
                Slider.maxValue = (MediaPlayerCtrl.GetDuration() / 1000f);
                var totalTime = TimeSpan.FromSeconds(Slider.maxValue);
                EndTime.GetComponent<TextMeshProUGUI>().text = string.Format("{0}:{1:00}", (int)totalTime.TotalMinutes, totalTime.Seconds);
                // var totalTime = TimeSpan.FromSeconds(Slider.maxValue);
                //  GameObject.Find("EndTime").GetComponent<Text>().text = string.Format("{0}:{1:00}", (int)totalTime.TotalMinutes, totalTime.Seconds);
            }
           
            Slider.value = MediaPlayerCtrl.GetSeekPosition() / 1000f;
            var time = TimeSpan.FromSeconds(MediaPlayerCtrl.GetSeekPosition() / 1000f);
            StartTime.GetComponent<TextMeshProUGUI>().text = string.Format("{0}:{1:00}", (int)time.TotalMinutes, time.Seconds);
            // var time = TimeSpan.FromSeconds(MediaPlayerCtrl.GetSeekPosition() / 1000f);
            //  GameObject.Find("StartTime").GetComponent<Text>().text = string.Format("{0}:{1:00}", (int)time.TotalMinutes, time.Seconds);
        }

        private IEnumerator FadeToMenu() {
            // Wait for the screen to fade out.
            yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
            // Load the main menu by itself.
            if (HomeInitializer.ChannelClicked.Equals(Constants.YoutubeChannelClicked))
                SceneManager.LoadScene(Constants.ChannelScene, LoadSceneMode.Single);
            else {
                SceneManager.LoadScene(Constants.ChannelSdCardScene, LoadSceneMode.Single);
            }
        }

        public void DisplayError() {
            Errortext.SetActive(true);
        }

        public void ChangeResolution(int objectResolution) {
            _currentResolution = objectResolution;
            StartCoroutine(ChangeResolution());
        }


        public void SetResolution(int objectResolution) {
            _currentResolution = objectResolution;
        }


        private IEnumerator ChangeResolution() {
            _isResChanged = true;
            _currentSliderValue = Slider.value;
            //  StartCoroutine(ShowLoadingIcon());
            //  yield return new WaitForSeconds(1);
            yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
            var videoFile = GetVideoFileName();
            //MediaPlayerCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName = videoFile;
            MediaPlayerCtrl.Load(videoFile);
            MediaPlayerCtrl.Play();
            yield return StartCoroutine(_vrCameraFade.BeginFadeIn(true));
            //  yield return new WaitForSeconds(1);
            MediaPlayerCtrl.SeekTo((int)_currentSliderValue * 1000);


        }
    }
}
