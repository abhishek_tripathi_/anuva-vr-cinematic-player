﻿using Assets.Scripts.channel;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.video {
    public class VideoInforamtion : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        public bool _gazeOver;

        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public VideoSelector VideoSelector;
        private GameObject[] VideoParentGameObject;
        private GameObject VideoInfoParentObject;

        private void Awake() {
            //VideoPanelParentObject.SetActive(false);
            VideoParentGameObject = new GameObject[4];
            VideoParentGameObject[0] = GameObject.Find("VIDEOS");
            VideoParentGameObject[1] = GameObject.Find("Video");
            VideoParentGameObject[2] = GameObject.Find("PLAYLISTINFO");
           // VideoParentGameObject[3] = GameObject.Find("PLAYLIST");
            VideoParentGameObject[3] = GameObject.Find("Playlist");
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            VideoInfoParentObject = VideoParentGameObject[0].GetComponent<VideoInitializer>().VideoInfoParentObject;
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.6f, 0.6f, 0.6f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            VideoSelector.isToggleObjectsSelected = true;
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            VideoSelector.isToggleObjectsSelected = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

      

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;
            HandleOut();
            foreach (var videoPanelParent in VideoParentGameObject) {
                if(videoPanelParent != null)
                videoPanelParent.SetActive(false);
            }
            foreach (var toggleVisibleObj in VideoSelector.ToggleVisibleObjects) {
                toggleVisibleObj.SetActive(false);
            }
            ChannelInitializer.Is360Video = VideoSelector.Is360Video;
            VideoInfoParentObject.SetActive(true);
            VideoInfoInitializer.duration = VideoSelector.Duration;
            VideoInfoInitializer.VideoId = VideoSelector.VideoId;
           // VideoInfoInitializer.desc = "Lorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex. At dolor aliquid constituto per. Percipit Lorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex. At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.Percipit asdasdLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex. At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.PercipitLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex.At dolor aliquid constituto per.Percipit asdasdLorem ipsum dolor sit amet, eum diam assentior tincidunt cu, per primis blandit ex. At dolor aliquid constituto per.Percipit asdasd";
            VideoInfoInitializer.desc = VideoSelector.Desc;
            VideoInfoInitializer.dislikes = VideoSelector.Dislikes;
            VideoInfoInitializer.likes = VideoSelector.Likes;
            VideoInfoInitializer.title = VideoSelector.OrigTitle;
            VideoInfoInitializer.views = VideoSelector.Views;
            VideoInfoInitializer.VideoImage = VideoSelector.Image;
           
            VideoInfoInitializer.isFirst = true;
        }

    }
}
