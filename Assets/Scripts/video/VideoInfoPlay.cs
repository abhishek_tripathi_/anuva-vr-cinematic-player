﻿using System.Collections;
using Assets.Scripts.channel;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts.video {
    public class VideoInfoPlay : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        public bool _gazeOver;
        private VRCameraFade _vrCameraFade;

        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public GameObject VideoInfoParentGameObject;

        private void Awake() {
            _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.8f, 0.8f, 0.8f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
            _vrCameraFade.OnFadeComplete += HandleFadeComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
            _vrCameraFade.OnFadeComplete -= HandleFadeComplete;

        }

        private void HandleOver() {
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private IEnumerator FadeToMenu() {
            ChannelInitializer.VideoId = VideoInfoInitializer.VideoId;
            // Wait for the screen to fade out.
            yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
            // Load the main menu by itself.
           // SceneManager.LoadScene(ChannelInitializer.Is360Video ? Constants.VrVideoScene : Constants.NonVrScene, LoadSceneMode.Single);
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;
            HandleOut();
           // ChannelInitializer.VideoId= VideoInfoInitializer.VideoId;
            //SceneManager.LoadScene(ChannelInitializer.Is360Video ? Constants.VrVideoScene : Constants.NonVrScene, LoadSceneMode.Single);
           
            StartCoroutine(FadeToMenu());
        }

        private void HandleFadeComplete() {
            SceneManager.LoadScene(ChannelInitializer.Is360Video ? Constants.VrVideoScene : Constants.NonVrScene, LoadSceneMode.Single);
        }

    }
}
