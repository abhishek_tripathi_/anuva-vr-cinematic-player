﻿using Assets.Scripts.video;
using TMPro;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.imagetext {
    public class PreviousText : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        [HideInInspector]
        public bool _gazeOver;

        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public VideoInfoInitializer VideoInfoInit;

        private void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.6f, 0.6f, 0.6f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;
            HandleOut();
            VideoInfoInitializer.currentChunk--;
            VideoInfoInit.DescObject.GetComponent<TextMeshProUGUI>().text = VideoInfoInit.descChunks[VideoInfoInitializer.currentChunk -1];
           
        }
    }
}
