﻿using System;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.channel_sdcard.ChannelHome {
    public class SdCardVideoInfoInitializer : MonoBehaviour {

        public static string title;
        public static string duration;
        public static string VideoId;

        public GameObject TitleGameObject;
        public GameObject ResolutionObject;
        public GameObject DurationObject;
        public static Sprite VideoImage;
        public GameObject ImageObject;
        public GameObject MoveObject;
        public GameObject SetDefaultRootBtn;

        void OnEnable() {
            MoveObject.transform.position = new Vector3(0, 0, MoveObject.transform.position.z);
            SetDefaultRootBtn.SetActive(false);
            SetMetadataOfVideoFile();
        }

        private void SetMetadataOfVideoFile() {
            TitleGameObject.GetComponent<TextMeshProUGUI>().text = title;
            SetDurationOfVideoFile();
          //  SetResolutionOfVideoFile();
            SetImageOfFile();
        }

        private void SetResolutionOfVideoFile() {
            var height = GetMetaInfoOfFile(19);
            var width = GetMetaInfoOfFile(18);
            ResolutionObject.GetComponent<TextMeshProUGUI>().text = width + " X " + height;
        }

        private void SetDurationOfVideoFile() {
             duration = GetDurationOfVideoFile();
            DurationObject.GetComponent<TextMeshProUGUI>().text = duration;
        }

        private string GetDurationOfVideoFile() {
            var metaCode = 9;
            var metaInfo = GetMetaInfoOfFile(metaCode);///////// duration string is in milliseconds.
            int totalSeconds = Convert.ToInt32(metaInfo)/1000;
            int seconds = totalSeconds % 60;
            int minutes = totalSeconds / 60;
            metaInfo = string.Format("{0}:{1:00}", minutes, seconds);
            return metaInfo;
        }

        private static string GetMetaInfoOfFile(int metaCode) {
            var metadataObj = new AndroidJavaObject("android.media.MediaMetadataRetriever");
            metadataObj.Call("setDataSource", VideoId);
            var metaInfo = metadataObj.Call<string>("extractMetadata", metaCode);
            return metaInfo;
        }

        private void SetImageOfFile() {
                var metadataObj = new AndroidJavaObject("android.media.MediaMetadataRetriever");
                metadataObj.Call("setDataSource", VideoId);
                var bitMap = metadataObj.Call<AndroidJavaObject>("getFrameAtTime");
            if (bitMap == null)
                return;
                byte[] m_image;
                AndroidJavaObject bitmapCompressFormat = new AndroidJavaClass("android/graphics/Bitmap$CompressFormat").GetStatic<AndroidJavaObject>("JPEG");
                var byteArrayOutStream = new AndroidJavaObject("java.io.ByteArrayOutputStream");
                bitMap.Call<bool>("compress", bitmapCompressFormat,100, byteArrayOutStream);
                m_image = byteArrayOutStream.Call<byte[]>("toByteArray");
                Texture2D texture = new Texture2D(100, 100);
                texture.LoadImage(m_image);
                var rec = new Rect(0, 0, texture.width, texture.height);
                ImageObject.GetComponent<Image>().sprite = Sprite.Create(texture, rec, new Vector2(0, 0), 1);

        }

    }
}
