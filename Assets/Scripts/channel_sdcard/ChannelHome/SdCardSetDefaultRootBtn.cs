﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts.channel_sdcard.ChannelHome {
    public class SdCardSetDefaultRootBtn : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        private bool _gazeOver;
        private VRCameraFade _vrCameraFade;

        void Awake() {
            _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _gazeOver = false;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
         //     _vrCameraFade.OnFadeComplete += HandleFadeComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
            //  _vrCameraFade.OnFadeComplete -= HandleFadeComplete;
        }

        private void HandleOver() {
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
        }

        private void HandleOut() {
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;
            HandleOut();

            SceneManager.LoadScene(Constants.SdCardDefaultRootScene, LoadSceneMode.Single);
         //   StartCoroutine(FadeToMenu());
        }

        //private IEnumerator FadeToMenu() {
        //    yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
        //}

        //private void HandleFadeComplete() {
        //    SceneManager.LoadScene(Constants.SdCardDefaultRootScene, LoadSceneMode.Single);
        //}

    }
}
