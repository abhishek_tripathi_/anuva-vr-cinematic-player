﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.channel_sdcard.ChannelHome {
    public class SdCardVideoInitializer : MonoBehaviour {

        private Vector3[] _prefabPos = {

           new Vector3(-436, 305, -0),               ///// Image 1
            new Vector3(-70,305, -0),                         ///// Image 2
            new Vector3(295,305, -0),
            new Vector3(664,305, -0),           ///// Image 3

            new Vector3(-436, 72, -0),               ///// Image 1
            new Vector3(-70,72, -0),                         ///// Image 2
            new Vector3(295,72, -0),
            new Vector3(664,72, -0),           ///// Image 3


            new Vector3(-436, -162, -0),               ///// Image 1
            new Vector3(-70,-162, -0),                         ///// Image 2
            new Vector3(295,-162, -0),
            new Vector3(664,-162, -0),           ///// Image 3


           new Vector3(-436, -397, -0),               ///// Image 1
            new Vector3(-70,-397, -0),                         ///// Image 2
            new Vector3(295,-397, -0),
            new Vector3(664,-397, -0),

        };

        private Vector3[] _prefabRot = {

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),
        };
        private int _gridCount = 16;
        private GameObject _parentObj = null;
        private bool _isAllLoaded;
        public GameObject LoadingIcon;
        public GameObject LoadingText;

        public GameObject ParentPrefab;
        public GameObject DefaultRootMp4VideoPrefab;
        static private int currentCount = 0;
        private int _parentCount;
        private float cameraDistanceToMove = 46.9f;
        public GameObject VideoPanelParentObj;
        public SdCardChannelInitializer ChannelInit;
        public GameObject SetDefaultRootParentObject;
        public GameObject VideoInfoParentObject;
        private int _subDirectoryCount;

        void OnEnable() {
            if (SdCardChannelInitializer.IsBackPressed)
                return;

            InitLoad();
        }

        public void InitLoad() {
            currentCount = 0;
            _parentCount = 1;
            _subDirectoryCount = 0;
            _isAllLoaded = false;
            DestroyAllVideoChild();
            SetDefaultRootParentObject.SetActive(false);
            VideoPanelParentObj.SetActive(false);
            if (string.IsNullOrEmpty(SdCardChannelInitializer.DefaultRootAbsolutePath)) {
                ShowSetDefaultRootPathText();
            }
            else {
                StartCoroutine(ShowLoadingIcon());
                StartCoroutine(LoadAllDefaultRootFiles());
            }
        }

        private void ShowSetDefaultRootPathText() {
            SetDefaultRootParentObject.SetActive(true);
        }

        private IEnumerator ShowLoadingIcon() {
            LoadingIcon.SetActive(true);
            LoadingText.SetActive(true);
            while (!_isAllLoaded || _subDirectoryCount!=0) {
                yield return null;
            }
            LoadingIcon.SetActive(false);
            LoadingText.SetActive(false);
        }

        void Update() {
            if (_isAllLoaded) {
                SdCardChannelInitializer.VideolistMaxValue = (_parentCount - 2) * cameraDistanceToMove;
            }
        }

        private void DestroyAllVideoChild() {
            foreach (Transform child in VideoPanelParentObj.transform) {
                Destroy(child.gameObject);
            }
        }

        private IEnumerator LoadAllDefaultRootFiles() {
            var fileObject = new AndroidJavaObject("java.io.File", SdCardChannelInitializer.DefaultRootAbsolutePath);
            var filesList = fileObject.Call<string[]>("list");
           
            
            yield return null;

            foreach (string fileName in filesList) {
                var file = new AndroidJavaObject("java.io.File", SdCardChannelInitializer.DefaultRootAbsolutePath + "/" + fileName);
                var isDirectory = file.Call<bool>("isDirectory");
                if (isDirectory) {
                    _subDirectoryCount++;
                    StartCoroutine(LoadAllDefaultRootFilesFromFolder(SdCardChannelInitializer.DefaultRootAbsolutePath + "/" + fileName));
                } else {

                    /////////check for valid video file ...mp4
                    if (!(fileName.EndsWith(".mp4")))
                        continue;  /////// continue if it is not a mp4 file.

                    if (((currentCount + 1)%_gridCount) == 1)
                        _parentObj = InstantiateParentPrefab();
                    var fileObj = Instantiate(DefaultRootMp4VideoPrefab);
                    fileObj.transform.parent = _parentObj.transform;
                    var filePos = _prefabPos[(currentCount%_gridCount)];
                    fileObj.GetComponent<RectTransform>().transform.localPosition = filePos;
                    fileObj.GetComponent<RectTransform>().transform.localScale = new Vector3(1, 1, 1);
                    fileObj.GetComponent<SdCardFileSelector>().FilePath = fileName;
                    fileObj.GetComponent<SdCardFileSelector>().AbsoluteFilePath = SdCardChannelInitializer.DefaultRootAbsolutePath + "/" + fileName;
                   //var sprite = SetFileImage(SdCardChannelInitializer.DefaultRootAbsolutePath + "/" + fileName);
                   // if (sprite != null)
                   //     fileObj.GetComponent<Image>().sprite = sprite;
                   currentCount++;
                    yield return null;
                }
            }

            VideoPanelParentObj.SetActive(true);
            _isAllLoaded = true;
        }

        private IEnumerator LoadAllDefaultRootFilesFromFolder(string dir) {
            var fileObject = new AndroidJavaObject("java.io.File",dir);
            var filesList = fileObject.Call<string[]>("list");
            yield return null;

            foreach (string fileName in filesList) {
                var file = new AndroidJavaObject("java.io.File", dir + "/" + fileName);
                var isDirectory = file.Call<bool>("isDirectory");
                if (isDirectory) {
                    _subDirectoryCount++;
                    StartCoroutine(LoadAllDefaultRootFilesFromFolder(dir + "/" + fileName));
                }
                else {
                    /////////check for valid video file ...mp4
                    if (!(fileName.EndsWith(".mp4")))
                        continue;  /////// continue if it is not a mp4 file.

                    if (((currentCount + 1) % _gridCount) == 1)
                        _parentObj = InstantiateParentPrefab();
                    var fileObj = Instantiate(DefaultRootMp4VideoPrefab);
                    fileObj.transform.parent = _parentObj.transform;
                    var filePos = _prefabPos[(currentCount % _gridCount)];
                    fileObj.GetComponent<RectTransform>().transform.localPosition = filePos;
                    fileObj.GetComponent<RectTransform>().transform.localScale = new Vector3(1, 1, 1);
                    fileObj.GetComponent<SdCardFileSelector>().FilePath = fileName;
                    fileObj.GetComponent<SdCardFileSelector>().AbsoluteFilePath = dir + "/" + fileName;
                    //var sprite = SetFileImage(dir + "/" + fileName);
                    //if(sprite !=null)
                    //    fileObj.GetComponent<Image>().sprite = sprite;
                    currentCount++;
                    yield return null;
                }
            }

            VideoPanelParentObj.SetActive(true);
            _subDirectoryCount--;
            _isAllLoaded = true;
        }

        //private Sprite SetFileImage(string absoluteFilePath) {
        //    try {
        //        var metadataObj = new AndroidJavaObject("android.media.MediaMetadataRetriever");
        //        metadataObj.Call("setDataSource", absoluteFilePath);
        //         var bitMap = metadataObj.Call<AndroidJavaObject>("getFrameAtTime");
        //        if (bitMap == null)
        //            return null;
        //        byte[] m_image;
        //       var  bitmapCompressFormat = new AndroidJavaClass("android/graphics/Bitmap$CompressFormat").GetStatic<AndroidJavaObject>("JPEG");
        //        var byteArrayOutStream = new AndroidJavaObject("java.io.ByteArrayOutputStream");
        //        bitMap.Call<bool>("compress", bitmapCompressFormat, 100, byteArrayOutStream);
        //        m_image = byteArrayOutStream.Call<byte[]>("toByteArray");
        //        var texture = new Texture2D(100, 100);
        //        texture.LoadImage(m_image);
        //        var rec = new Rect(0, 0, texture.width, texture.height);
        //        return Sprite.Create(texture, rec, new Vector2(0, 0), 1);
        //    }
        //    catch (Exception ex) {
        //        ChannelInit.InfoTextObject.GetComponent<TextMeshProUGUI>().text = ex.GetBaseException().Message;
        //    }
        //    return null;
        //}

        private Sprite SetFileImage(string absoluteFilePath) {
            try {
                var metadataObj = new AndroidJavaObject("android.media.MediaMetadataRetriever");
                metadataObj.Call("setDataSource", absoluteFilePath);
                var bitMap = metadataObj.Call<AndroidJavaObject>("getFrameAtTime");
                if (bitMap == null)
                    return null;
                var rowBytes = bitMap.Call<int>("getRowBytes");
                var height = bitMap.Call<int>("getHeight");
                // var size = rowBytes*height;
                var size = bitMap.Call<int>("getByteCount");
              //  ChannelInit.InfoTextObject.GetComponent<TextMeshProUGUI>().text += "size=" + size + "~~";
                AndroidJavaObject byteBuffer = new AndroidJavaClass("java.nio.ByteBuffer").CallStatic<AndroidJavaObject>("allocate", size);
                bitMap.Call("copyPixelsToBuffer", byteBuffer);
                byteBuffer.Call("rewind");

                /*byte[] m_image1 = byteBuffer.Call<byte[]>("array");
                byte[] m_image = new byte[m_image1.Length];
                for (int i = 0; i < m_image1.Length; i++)
                    m_image[i] = byteBuffer.Call<byte>("get");
                // byteBuffer.Call("get",m_image,0,size);
                */

                var  m_image = byteBuffer.Call<byte[]>("array");


                //byte[] m_image;
                //var bitmapCompressFormat = new AndroidJavaClass("android/graphics/Bitmap$CompressFormat").GetStatic<AndroidJavaObject>("JPEG");
                //var byteArrayOutStream = new AndroidJavaObject("java.io.ByteArrayOutputStream");
                //bitMap.Call<bool>("compress", bitmapCompressFormat, 100, byteArrayOutStream);
                //m_image = byteArrayOutStream.Call<byte[]>("toByteArray");



                var texture = new Texture2D(100, 100);
                texture.LoadImage(m_image);
                var rec = new Rect(0, 0, texture.width, texture.height);
                return Sprite.Create(texture, rec, new Vector2(0, 0), 1);
            }
            catch (Exception ex) {
                ChannelInit.InfoTextObject.GetComponent<TextMeshProUGUI>().text += ex.GetBaseException().Message+"~~";
            }
            return null;
        }

        private GameObject InstantiateParentPrefab() {
            var parentObject = Instantiate(ParentPrefab);
            parentObject.transform.position = Vector3.zero;
            PlacePrefabInWorld(parentObject);
            return parentObject;
        }

        private void PlacePrefabInWorld(GameObject parentObject) {
            var posX = 0f;
            posX = (_parentCount - 1) * cameraDistanceToMove;
            parentObject.transform.parent = VideoPanelParentObj.transform;
            parentObject.GetComponent<RectTransform>().transform.localPosition = new Vector3(posX, 0, 16.1f);
            _parentCount++;
        }

    }
}
