﻿using TMPro;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts.channel_sdcard.ChannelHome {
    public class SdCardFileSelector : MonoBehaviour {

        public VRInteractiveItem VrItem;
        public GameObject TitleGameObject;
        [HideInInspector]
        public string FilePath;
        [HideInInspector] public string AbsoluteFilePath;
        public GameObject[] ToggleVisibleObjects;
        [HideInInspector]
        public bool IsToggleObjectsSelected;

        void Start() {
            IsToggleObjectsSelected = false;
            TitleGameObject.GetComponent<TextMeshProUGUI>().text = FilePath;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
        }

        private void HandleOver() {
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(true);
            }
        }

        private void HandleOut() {
            if (IsToggleObjectsSelected)
                return;
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(false);
            }
        }
    }
}
