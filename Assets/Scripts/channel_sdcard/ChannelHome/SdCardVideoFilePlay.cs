﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts.channel_sdcard.ChannelHome {
    public class SdCardVideoFilePlay : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        private bool _gazeOver;
        private VRCameraFade _vrCameraFade;
        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public SdCardFileSelector FileSelector;
        private GameObject _videoInfoParent;

        private void Awake() {
            _videoInfoParent = GameObject.Find("VIDEOS").GetComponent<SdCardVideoInitializer>().VideoInfoParentObject;
            _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.6f, 0.6f, 0.6f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
         //   _vrCameraFade.OnFadeComplete += HandleFadeComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
      //      _vrCameraFade.OnFadeComplete -= HandleFadeComplete;
        }

        private void HandleOver() {
            FileSelector.IsToggleObjectsSelected = true;
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            FileSelector.IsToggleObjectsSelected = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;
            HandleOut();

            SdCardVideoInfoInitializer.title = FileSelector.FilePath;
            SdCardVideoInfoInitializer.VideoId = FileSelector.AbsoluteFilePath;
            GameObject.Find("Video").SetActive(false);
            _videoInfoParent.SetActive(true);
        }

        //private void HandleSelectionComplete() {
        //    if (!_gazeOver)
        //        return;
        //    HandleOut();
        //    SdCardChannelInitializer.VideoId = FileSelector.AbsoluteFilePath;
        //    SceneManager.LoadScene(Constants.NonVrScene, LoadSceneMode.Single);
        //    //   StartCoroutine(FadeToMenu());
        //}

        //private IEnumerator FadeToMenu() {
        //    SdCardChannelInitializer.VideoId = FileSelector.AbsoluteFilePath;
        //    yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
        //}

        //private void HandleFadeComplete() {
        //    SceneManager.LoadScene(Constants.NonVrScene, LoadSceneMode.Single);
        //}
    }
}
