﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.channel_sdcard.ChannelHome {
    public class SdCardCameraMovement : MonoBehaviour {

        public GameObject MoveObject;
        public GameObject VideoObject;
        [HideInInspector]
        public int ArrowClicked;   // 1 for right, -1 for left
        [HideInInspector]
        public bool IsMoving;
        private Vector3 _currentPos;
        private Vector3 _targetPos;
        private Vector3 _currentPosVideo;
        private Vector3 _targetPosVideo;
        public float moveSpeed = 0.1f;
        public float videoPlaneDistance = 46.9f;
        private bool coroutineStarted;
        public float TimeToWaitForSlide;
      

        void Awake() {
            ArrowClicked = 0;
            IsMoving = false;
            coroutineStarted = false;
        }

        void Update() {
            if (ArrowClicked != 0) {
                _currentPos = MoveObject.transform.position;
                    _targetPos = new Vector3(_currentPos.x + (ArrowClicked * videoPlaneDistance), _currentPos.y, _currentPos.z);
                    _targetPosVideo = _currentPosVideo = VideoObject.transform.position;
                ArrowClicked = 0;
            }
            if (IsMoving)
                UpdateMovement();
        }

        private void UpdateMovement() {
            MoveObject.transform.position = Vector3.MoveTowards(_currentPos, _targetPos, moveSpeed);
            //if (isVideoListToBeMoved)
            //    PlaylistObject.transform.position = Vector3.MoveTowards(_currentPosPList, _targetPosPList, moveSpeed);
            //else {
            //    VideoObject.transform.position = Vector3.MoveTowards(_currentPosVideo, _targetPosVideo, moveSpeed);
            //    PlaylistInfoGameObject.transform.position = Vector3.MoveTowards(_currentPosPInfo, _targetPosPInfo,
            //        moveSpeed);
            //}
            _currentPos = MoveObject.transform.position;
            _currentPosVideo = VideoObject.transform.position;

            if (_currentPos.Equals(_targetPos) && _currentPosVideo.Equals(_targetPosVideo)) {
                if (!coroutineStarted)
                    StartCoroutine(WaitForSomeTime());
            }
        }

        private IEnumerator WaitForSomeTime() {
            coroutineStarted = true;
            yield return new WaitForSeconds(TimeToWaitForSlide);
            IsMoving = false;
            coroutineStarted = false;
        }
    }
}
