﻿using System.Collections;
using Assets.Scripts.channel;
using Assets.Scripts.channel_sdcard.ChannelHome;
using Assets.Scripts.home;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts {
    public class PreviousBtn : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        //private VRCameraFade _vrCameraFade;
        public GameObject ReticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public GameObject ChannelInitializer;
        private bool _isGazeOver;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
           // _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(1, 1, 1);
            _reticleTransform = ReticleGameObject.GetComponent<RectTransform>();
            _isGazeOver = false;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionCompletePrevious += HandleSelectionComplete;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionCompletePrevious -= HandleSelectionComplete;
        }

        private void HandleOver() {
            _isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
            _selectionRadial.isPrevClicked = true;
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _selectionRadial.isPrevClicked = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;
            HandleBackEvent();
        }

        private void HandleBackEvent() {
            if(HomeInitializer.ChannelClicked.Equals(Constants.YoutubeChannelClicked))
                HandleBackEventForYoutubeChannel();
            else {
                HandleBackEventForSdCardChannel();
            }
        }

        private void HandleBackEventForYoutubeChannel() {
            if (ChannelInitializer == null) {
                channel.ChannelInitializer.IsBackPressedInDifferentScene = true;
                SceneManager.LoadScene(Constants.ChannelScene, LoadSceneMode.Single);
            }
            var channelInitializer = ChannelInitializer.GetComponent<ChannelInitializer>();
            if (VideoInitializer.isMyVideosPlaylist) {
                channelInitializer.VideoCategoryObject.SetActive(true);
                channelInitializer.VideoObject.SetActive(true);
                channelInitializer.VideoInfoObject.SetActive(false);
                channelInitializer.VideoObject.transform.position = GetOriginalPosition(channelInitializer.VideoObject, 0, false);
                channelInitializer.PlaylistObject.transform.position = GetOriginalPosition(channelInitializer.PlaylistObject, 0, false);
                channelInitializer.PlaylistInfoObject.GetComponent<RectTransform>().localPosition = GetOriginalPosition(channelInitializer.PlaylistInfoObject, channelInitializer.PlaylistInfoXPos, true);
                channel.ChannelInitializer.IsBackPressed = true;
                HandleOut();
                return;
            }
            if (channelInitializer.VideoInfoObject.activeInHierarchy) {
                ////////  if video info is active
                channelInitializer.VideoInfoObject.SetActive(false);
                channelInitializer.VideoCategoryObject.SetActive(true);
                channelInitializer.VideoObject.SetActive(true);
                channelInitializer.VideoObject.transform.position = GetOriginalPosition(channelInitializer.VideoObject, 0, false);
                channelInitializer.PlaylistCategoryObject.SetActive(true);
                channelInitializer.PlaylistObject.SetActive(true);
                channelInitializer.PlaylistObject.transform.position = GetOriginalPosition(channelInitializer.PlaylistObject, 0, false);
                channelInitializer.PlaylistInfoObject.SetActive(true);
                channelInitializer.PlaylistInfoObject.GetComponent<RectTransform>().localPosition = GetOriginalPosition(channelInitializer.PlaylistInfoObject, channelInitializer.PlaylistInfoXPos, true);
                channel.ChannelInitializer.IsBackPressed = true;
                HandleOut();
            }
            else {
                HandleOut();
            }
        }

        private void HandleBackEventForSdCardChannel() {
            if (ChannelInitializer == null) {
                SceneManager.LoadScene(Constants.ChannelSdCardScene, LoadSceneMode.Single);
            }
            var channelInitializer = ChannelInitializer.GetComponent<SdCardChannelInitializer>();
            if (channelInitializer.VideoInfoObject.activeInHierarchy) {
                ////////  if video info is active
                channelInitializer.VideoInfoObject.SetActive(false);
                channelInitializer.VideoCategoryObject.SetActive(true);
                channelInitializer.VideoObject.SetActive(true);
                channelInitializer.VideoObject.transform.position = GetOriginalPosition(channelInitializer.VideoObject, 0, false);
                SdCardChannelInitializer.IsBackPressed = true;
                HandleOut();
            }
            else {
                HandleOut();
            }

        }

        private Vector3 GetOriginalPosition(GameObject targetObj, float originalPosInX, bool isChild) {
            if(isChild)
                return new Vector3(originalPosInX,targetObj.GetComponent<RectTransform>().localPosition.y,targetObj.GetComponent<RectTransform>().localPosition.z);
            return new Vector3(originalPosInX, targetObj.transform.position.y, targetObj.transform.position.z);
        }

    }
}
