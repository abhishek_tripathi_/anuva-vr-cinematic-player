﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using SimpleJSON;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.playlist {
    public class PlaylistInfoInitializer : MonoBehaviour {

        public static string desc;
        public static string title;
        public static string totalVideos;
        public static string PlaylistId;
        public GameObject DescObject;
        public GameObject TitleGameObject;
        public static Sprite PlaylistImage;
        public GameObject ImageObject;
        public GameObject OwnerText;
        public GameObject TotalVideos;
        public string channelName;
        private bool isHighResImageLoaded;

        public static bool isFirst = true;
        //public GameObject MoveObject;

        void Start() {
            //DescObject.GetComponent<Text>().text = desc;
            //TitleGameObject.GetComponent<Text>().text = title;
            //ImageObject.GetComponent<Image>().sprite = PlaylistImage;
            //PlaylistImage = null;
        }

        void OnEnable() {
         //   MoveObject.transform.position = new Vector3(0, 0, MoveObject.transform.position.z);
            channelName = null;
            isHighResImageLoaded = false;
        }

        void Update() {

            if (!isFirst) {
                TotalVideos.GetComponent<TextMeshProUGUI>().text = totalVideos;
                return;
            }

            StartCoroutine(GetChannelId());
            if (!string.IsNullOrEmpty(desc))
                desc = Regex.Replace(desc, @"[^\u0000-\u007F]", string.Empty);
            DescObject.GetComponent<TextMeshProUGUI>().text = desc;
            if (!string.IsNullOrEmpty(title))
                title = Regex.Replace(title, @"[^\u0000-\u007F]", string.Empty);
            TitleGameObject.GetComponent<TextMeshProUGUI>().text = title;
            TotalVideos.GetComponent<TextMeshProUGUI>().text = totalVideos;
            OwnerText.GetComponent<TextMeshProUGUI>().text = channelName;
            ImageObject.GetComponent<Image>().sprite = PlaylistImage;
            isFirst = false;
        }


        IEnumerator GetChannelId() {
            var YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
            var ncal = new WWW("https://www.googleapis.com/youtube/v3/playlists?key=" + YourAPIKey + "&part=snippet&id=" + PlaylistId);
            yield return ncal;
            var youtubeNewReturn = JSON.Parse(ncal.text);
            youtubeNewReturn = youtubeNewReturn["items"];
            StartCoroutine(DownloadThumbs(youtubeNewReturn[0]["snippet"]["thumbnails"]["standard"]["url"]));
            var channelId = youtubeNewReturn[0]["snippet"]["channelId"];
            if (string.IsNullOrEmpty(channelId)) {
                OwnerText.GetComponent<TextMeshProUGUI>().text =channelName = "ANUVA VR";
                yield return null;
            }
            StartCoroutine(GetChannelInfo(channelId));
        }

        IEnumerator GetChannelInfo(string channelId) {
            var YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
            var ncal = new WWW("https://www.googleapis.com/youtube/v3/channels?key=" + YourAPIKey + "&part=snippet&id=" + channelId);
            yield return ncal;
            var youtubeNewReturn = JSON.Parse(ncal.text);
            youtubeNewReturn = youtubeNewReturn["items"];
            if (youtubeNewReturn == null)
                yield return null;
            OwnerText.GetComponent<TextMeshProUGUI>().text = channelName = youtubeNewReturn[0]["snippet"]["title"];
            // var channelId = youtubeNewReturn[0]["snippet"]["channelId"];
        }

        private IEnumerator DownloadThumbs(string url) {

            var www = new WWW(url);
            yield return www;
           // if (www.error != null)
                yield return null;
                var texture = new Texture2D(100, 100);
                www.LoadImageIntoTexture(texture);
                //var rec = new Rect(0, 0, texture.width, texture.height);
                var rec = new Rect(0, 0, texture.width, texture.height);
                ImageObject.GetComponent<Image>().sprite = Sprite.Create(texture, rec, new Vector2(0, 0), 1);
            // isHighResImageLoaded = true;
        }
    }
}
