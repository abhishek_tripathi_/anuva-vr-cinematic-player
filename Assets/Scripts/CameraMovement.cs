﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class CameraMovement : MonoBehaviour {

        public GameObject MoveObject;
        public GameObject PlaylistObject;
        public GameObject VideoObject;
        public GameObject PlaylistInfoGameObject;
        [HideInInspector]
        public int ArrowClicked;   // 1 for right, -1 for left
        [HideInInspector]
        public bool IsMoving;
        private Vector3 _currentPos;
        private Vector3 _targetPos;
        private Vector3 _currentPosVideo;
        private Vector3 _targetPosVideo;
        private Vector3 _currentPosPList;
        private Vector3 _targetPosPList;
        private Vector3 _currentPosPInfo;
        private Vector3 _targetPosPInfo;
        public float moveSpeed = 0.1f;
        public  float videoPlaneDistance = 46.9f;
        public float playlistPlaneDistance = 43.5f;
        public GameObject GUIReticle;
        public GameObject BackgroundImage;
        public Sprite OriginalReticle;
        public Sprite LeftArrow;
        public Sprite RightArrow;
        private bool coroutineStarted;
        public float TimeToWaitForSlide;
        [HideInInspector]
        public bool isVideoListToBeMoved = false;

        void Awake() {
            ArrowClicked = 0;
            IsMoving = false;
            coroutineStarted = false;
        }

        void Update() {
            if (ArrowClicked != 0) {
                _currentPos = MoveObject.transform.position;
                if (isVideoListToBeMoved) {
                    _targetPos = new Vector3(_currentPos.x + (ArrowClicked*videoPlaneDistance), _currentPos.y,  _currentPos.z);
                    _currentPosPList = PlaylistObject.transform.position;
                    _targetPosPList = new Vector3(_currentPosPList.x + (ArrowClicked * videoPlaneDistance), _currentPosPList.y, _currentPosPList.z);
                    _targetPosVideo = _currentPosVideo = VideoObject.transform.position;
                    _targetPosPInfo = _currentPosPInfo = PlaylistInfoGameObject.transform.position;
                }
                else {
                    _targetPos = new Vector3(_currentPos.x + (ArrowClicked * playlistPlaneDistance), _currentPos.y, _currentPos.z);
                    _currentPosVideo = VideoObject.transform.position;
                    _currentPosPInfo = PlaylistInfoGameObject.transform.position;
                    _targetPosVideo = new Vector3(_currentPosVideo.x + (ArrowClicked * playlistPlaneDistance), _currentPosVideo.y, _currentPosVideo.z);
                    _targetPosPInfo = new Vector3(_currentPosPInfo.x + (ArrowClicked * playlistPlaneDistance), _currentPosPInfo.y, _currentPosPInfo.z);
                    _targetPosPList = _currentPosPList = PlaylistObject.transform.position;
                }
                //if (ArrowClicked == 1) {
                //    BackgroundImage.GetComponent<Image>().enabled = false;
                //    GUIReticle.GetComponent<Image>().sprite = RightArrow;
                //}
                //else if (ArrowClicked == -1) {
                //    BackgroundImage.GetComponent<Image>().enabled = false;
                //    GUIReticle.GetComponent<Image>().sprite = LeftArrow;
                //}
                ArrowClicked = 0;
            }
            if (IsMoving)
                UpdateMovement();
        }

        private void UpdateMovement() {
            MoveObject.transform.position = Vector3.MoveTowards(_currentPos, _targetPos, moveSpeed);
            if (isVideoListToBeMoved)
                PlaylistObject.transform.position = Vector3.MoveTowards(_currentPosPList, _targetPosPList, moveSpeed);
            else {
                VideoObject.transform.position = Vector3.MoveTowards(_currentPosVideo, _targetPosVideo, moveSpeed);
                PlaylistInfoGameObject.transform.position = Vector3.MoveTowards(_currentPosPInfo, _targetPosPInfo,
                    moveSpeed);
            }
            _currentPos = MoveObject.transform.position;
            _currentPosPList = PlaylistObject.transform.position;
            _currentPosVideo = VideoObject.transform.position;
            _currentPosPInfo = PlaylistInfoGameObject.transform.position;

            // if (_currentPos.Equals(_targetPos) && _currentPosPList.Equals(_targetPosPList) && _currentPosVideo.Equals(_targetPosVideo) && _currentPosPInfo.Equals(_targetPosPInfo))
            if (_currentPos.Equals(_targetPos) && _currentPosPList.Equals(_targetPosPList) &&  _currentPosVideo.Equals(_targetPosVideo)) {
                if (!coroutineStarted)
                    StartCoroutine(WaitForSomeTime());

                //IsMoving = false;
                //BackgroundImage.GetComponent<Image>().enabled = true;
                //GUIReticle.GetComponent<Image>().sprite = OriginalReticle;
            }
        }

        private IEnumerator WaitForSomeTime() {
            coroutineStarted = true;
            yield return new WaitForSeconds(TimeToWaitForSlide);
            IsMoving = false;
            //BackgroundImage.GetComponent<Image>().enabled = true;
            //GUIReticle.GetComponent<Image>().sprite = OriginalReticle;
            coroutineStarted = false;
        }
    }
}
