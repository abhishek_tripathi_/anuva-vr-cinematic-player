﻿
namespace Assets.Scripts {
    public interface IInitializer {

         void ChangeResolution(int resolution);

        void SetResolution(int objectResolution);

    }
}
