﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Assets.Scripts.channel;
using Assets.Scripts.playlist;
using SimpleJSON;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Channel_Umenu {
    public class PlaylistInitializer : MonoBehaviour {

        //private Vector3[] _prefabPos = {

        //    new Vector3(-3.339074f, 5.37f, -1.292746f),               ///// Image 1
        //    new Vector3(-2.61f,5.36f, -1.2f),                         ///// Image 2
        //    new Vector3(2.26f,5.36f, -1.2f),                         ///// Image 3
        //    new Vector3(2.83f,5.45f, -1.28f),                ///// Image 4

        //    new Vector3(-3.339074f,1.82f,-1.292746f),                ///// Image 5
        //    new Vector3(-2.61f,1.79f,-1.2f),                         ///// Image 6
        //    new Vector3(2.26f,1.79f,-1.2f),                          ///// Image 7
        //    new Vector3(2.83f,1.84f,-1.28f),                 ///// Image 8

        //    new Vector3(-3.339074f,-1.6f,-1.292746f),                ///// Image 9
        //    new Vector3(-2.61f,-1.64f,-1.2f),                       ///// Image 10
        //    new Vector3(2.26f,-1.64f,-1.2f),                        ///// Image 11
        //    new Vector3(2.83f,-1.61f,-1.28f)                ///// Image 12
        //};

        //private Vector3[] _prefabRot = {

        //    new Vector3(0,331.673f,0),
        //    new Vector3(0,0,0),
        //    new Vector3(0,0,0),
        //    new Vector3(0,28.9860f,0),

        //    new Vector3(0,331.673f,0),
        //    new Vector3(0,0,0),
        //    new Vector3(0,0,0),
        //    new Vector3(0,28.9860f,0),

        //    new Vector3(0,331.673f,0),
        //    new Vector3(0,0,0),
        //    new Vector3(0,0,0),
        //    new Vector3(0,28.9860f,0)
        //};

        private Vector3[] _prefabPos = {

            new Vector3(-441, 649, -0),               ///// Image 1
            new Vector3(-83,649, -0),                         ///// Image 2
            new Vector3(284,649, -0),
            new Vector3(655,649, -0),           ///// Image 3

          //  new Vector3(1013, 649, -0),               ///// Image 1
         //   new Vector3(1380,649, -0),                         ///// Image 2
           // new Vector3(295,72, -0),
           // new Vector3(664,72, -0),           ///// Image 3


           // new Vector3(-436, -162, -0),               ///// Image 1
           // new Vector3(-70,-162, -0),                         ///// Image 2
           // new Vector3(295,-162, -0),
           // new Vector3(664,-162, -0),           ///// Image 3


           //new Vector3(-436, -387, -0),               ///// Image 1
           // new Vector3(-70,-387, -0),                         ///// Image 2
           // new Vector3(295,-387, -0),
           // new Vector3(664,-387, -0),           ///// Image 3


        };

        private Vector3[] _prefabRot = {

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),
        };

        private int _gridCount = 4;
        private string YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
        private int maxresults = 50;
        GameObject parentObj = null;
        private string nextPageToken;
        private bool isFirstPage = true;

        public GameObject Prefab;
        public GameObject ParentPrefab;
        private bool isChannelSelected;
        static private int currentCount = 0;
        private int _parentCount;
        private float cameraDistanceToMove = 43.5f;
        public GameObject PlaylistPanelParentObj;
        public static string ChannelId;
        private List<string> playListIdsList;
        public  GameObject VideoPanelGameObject;
        public GameObject PlaylistInfoParentObject;
        public GameObject MoveObject;
        private string firstPlaylistId;
        private bool isAllListLoaded;

        public void OnEnable() {
            // PlaylistPanelParentObj = GameObject.Find("Playlist");
            firstPlaylistId = string.Empty;
            isFirstPage = true;
            nextPageToken = null;
            isChannelSelected = false;
            currentCount = 0;
            _parentCount = 1;
            playListIdsList = new List<string>();
            isAllListLoaded = false;
            MoveObject.transform.position = new Vector3(0,0,MoveObject.transform.position.z);
            PlaylistInfoInitializer.isFirst = true;
            VideoInitializer.isMyVideosPlaylist = false;

        }

        void Update() {
            if (ChannelInitializer.IsBackPressed)
                return;
            if (isAllListLoaded)
                return;

            if ((CheckNextTokenForNull(nextPageToken) || isFirstPage) && playListIdsList.Count < 50) {
                isFirstPage = false;
                YoutubeV3Call("video");
            }
            else if(!string.IsNullOrEmpty(firstPlaylistId) && !isAllListLoaded) {
                ChannelInitializer.PlaylistMaxValue = (_parentCount-2) * cameraDistanceToMove;
                VideoPanelGameObject.SetActive(true);
                PlaylistInfoParentObject.SetActive(true);
                VideoInitializer.PlaylistId = firstPlaylistId;
                ChannelInitializer.IsBackPressed = false;
                isAllListLoaded = true;

            }
        }

        public void YoutubeV3Call(string searchType) {
            if (nextPageToken == null) {
                StartCoroutine(PlaylistSearch("https://www.googleapis.com/youtube/v3/playlists?&key=" + YourAPIKey + "&part=snippet&&order=date&channelId=" + ChannelId + "&maxResults=" + maxresults + ""));
            }
            else if (nextPageToken != null) {
                StartCoroutine(PlaylistSearch("https://www.googleapis.com/youtube/v3/playlists?&key=" + YourAPIKey + "&part=snippet&channelId=" + ChannelId + "&pageToken=" + nextPageToken + ""));
            }
        }

        private IEnumerator PlaylistSearch(string url) {
            var call = new WWW(url);
            yield return call;
            var youtubeReturn = JSON.Parse(call.text);
            nextPageToken = youtubeReturn["nextPageToken"];
         //   var totalVideos = youtubeReturn["pageInfo"]["totalResults"].AsInt;
            youtubeReturn = youtubeReturn["items"];
            for (int i = youtubeReturn.Count-1; i >=0; i--) {
                if (((currentCount + 1) % _gridCount) == 1)
                    parentObj = InstantiateParentPrefab();
                if (!playListIdsList.Contains(youtubeReturn[i]["id"])) {
                    playListIdsList.Add(youtubeReturn[i]["id"]);
                    GetPlaylistInfo(youtubeReturn[i]["id"], youtubeReturn[i]["snippet"]["title"], youtubeReturn[i]["snippet"]["description"],youtubeReturn[i]["snippet"]["thumbnails"]["medium"]["url"], currentCount, parentObj);
                    currentCount++;
                    if (currentCount == 1) {
                        firstPlaylistId = youtubeReturn[i]["id"];
                      //  PlaylistInfoInitializer.totalVideos = totalVideos.ToString();
                        PlaylistInfoInitializer.PlaylistId = firstPlaylistId;
                        PlaylistInfoInitializer.title = youtubeReturn[i]["snippet"]["title"];
                        PlaylistInfoInitializer.desc = youtubeReturn[i]["snippet"]["description"];
                    }
                }
            }
        }

        private void GetPlaylistInfo(string playlistId, string title, string desc,string playListThumb,int count, GameObject ParentObject) {
            var imageObj = Instantiate(Prefab);
            imageObj.transform.parent = ParentObject.transform;
            var imagePos = _prefabPos[(count % _gridCount)];
            var imageRot = _prefabRot[(count % _gridCount)];
            //imageObj.transform.localPosition = imagePos;
            imageObj.GetComponent<RectTransform>().transform.localPosition = imagePos;
            imageObj.GetComponent<RectTransform>().transform.localScale = new Vector3(2,2,1.5f);
            if (!string.IsNullOrEmpty(title))
                title = Regex.Replace(title, @"[^\u0000-\u007F]", string.Empty);
            imageObj.GetComponentInChildren<TextMeshProUGUI>().text = title;
            imageObj.GetComponent<CursorSelector>().PlaylistId = playlistId;
            if(!string.IsNullOrEmpty(desc))
            desc = Regex.Replace(desc, @"[^\u0000-\u007F]", string.Empty);
            imageObj.GetComponent<CursorSelector>().Desc = desc;
            imageObj.GetComponent<CursorSelector>().Title = title;
            imageObj.GetComponent<CursorSelector>().VideoPanelParentObject = VideoPanelGameObject;
            if (currentCount == 0) {
                ChannelInitializer.CurrentActivePlaylist = imageObj;
                imageObj.GetComponent<CursorSelector>().PlaylistBG.SetActive(true);
                imageObj.GetComponent<Animator>().enabled = true;
            }
          //  imageObj.transform.localEulerAngles = imageRot;
           // StartCoroutine(DownloadThumbs(playListThumb, imageObj,playlistId));
        }

        private IEnumerator DownloadThumbs(string url, GameObject imageGameObject,string playlistId) {
            var www = new WWW(url);
            yield return www;
            var texture = new Texture2D(100, 100);
            www.LoadImageIntoTexture(texture);
            //var rec = new Rect(0, 0, texture.width, texture.height);
            var rec = new Rect(0, 0, texture.height, texture.height);

            imageGameObject.GetComponent<Image>().sprite = Sprite.Create(texture, rec, new Vector2(0, 0), 1);
            imageGameObject.GetComponent<CursorSelector>().Sprite = imageGameObject.GetComponent<Image>().sprite;
            if (playlistId.Equals(firstPlaylistId)) {
                //PlaylistInfoInitializer.PlaylistImage = imageGameObject.GetComponent<CursorSelector>().Sprite;
                PlaylistInfoParentObject.GetComponent<PlaylistInfoInitializer>().ImageObject.GetComponent<Image>().sprite = PlaylistInfoInitializer.PlaylistImage = imageGameObject.GetComponent<Image>().sprite; 

            }
            //  imageGameObject.GetComponentInChildren<CursorSelector>().Texture = texture;
            // imageGameObject.GetComponentInChildren<MeshRenderer>().material.mainTexture = texture;
        }

        private GameObject InstantiateParentPrefab() {
            var parentObject = Instantiate(ParentPrefab);
            parentObject.transform.position = Vector3.zero;
            PlacePrefabInWorld(parentObject);
            return parentObject;
        }

        private void PlacePrefabInWorld(GameObject parentObject) {
            var imagePos = parentObject.transform.position;
            var posX = 0f;
            //if (_parentCount % 2 == 0) { // if even
            //    posX = (_parentCount / 2) * cameraDistanceToMove;
            //}
            //else { // if odd
            //    posX = -(((_parentCount - 1) / 2) * cameraDistanceToMove);
            //}
            posX = (_parentCount - 1) * cameraDistanceToMove;
            parentObject.transform.parent = PlaylistPanelParentObj.transform;
            //parentObject.transform.localPosition = new Vector3(posX, imagePos.y, imagePos.z);
            parentObject.GetComponent<RectTransform>().transform.localPosition = new Vector3(posX, 0, 16.1f);
            _parentCount++;
        }

        private bool CheckNextTokenForNull(string tokenvalue) {
            return !string.IsNullOrEmpty(tokenvalue);
        }
    }
}
