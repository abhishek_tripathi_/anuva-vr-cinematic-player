﻿using System.Collections;
using UnityEngine;
using UnityEngine.VR;

namespace Assets.Scripts.splash {
    public class SplashController : MonoBehaviour {

        public float minimumTimeToShowLogo = 8f;
        public string levelToLoad = "";
	
        public new Animation animation;
        public GameObject LoadingGameObject;
        public GameObject NetworkErrorObj;
        public string introName = "SplashIn";
        public string outroName = "SplashOut";
	
        IEnumerator Start () {
		
            InputTracking.Recenter ();
		
            float minimumTimeEnd = Time.realtimeSinceStartup + minimumTimeToShowLogo;
		
            // intro
		
            animation.Play(introName);

            // background load the new scene (but don't activate it yet)
            //if (Application.internetReachability == NetworkReachability.NotReachable) {
            //    LoadingGameObject.SetActive(false);
            //    NetworkErrorObj.SetActive(true);
            //    yield return null;
            //}
            //else {
                AsyncOperation o = Application.LoadLevelAsync("LandingScene");
                o.allowSceneActivation = false;
                while (o.isDone) {
                    yield return new WaitForEndOfFrame();
                }

                // delay until minimum time is reached

                if (Time.realtimeSinceStartup < minimumTimeEnd) {
                    yield return new WaitForSeconds(minimumTimeEnd - Time.realtimeSinceStartup);
                }

                // outro

                AnimationClip clip = animation.GetClip(outroName);
                animation.Play(outroName);
                yield return new WaitForSeconds(clip.length);

                // activate scene

                o.allowSceneActivation = true;
           // }
        }
    }
}
