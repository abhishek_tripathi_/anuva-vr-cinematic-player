﻿using System;
using System.Collections;
using Assets.Scripts.channel;
using Assets.Scripts.channel_sdcard.ChannelHome;
using Assets.Scripts.home;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VRStandardAssets.Utils;

namespace Assets.Scripts {
    public class NonVrInit : MonoBehaviour,IInitializer{

        public MediaPlayerCtrl MediaCtrl;
        public Slider Slider;
        private VRCameraFade _vrCameraFade;
        public static bool _isGazeOver;
        public GameObject LoadingIcon;
       public GameObject LoadingText;
        public GameObject ZoomIn;
        public GameObject ZoomOut;
        public GameObject ZoomInGreyedOut;
        public GameObject ZoomOutGreyedOut;
        [HideInInspector]
        public int CurrentScreenPosIndex;
        public float[] _screenPosInZ;
        private int _currentResolution;
        private bool _isResChanged;
        private float _currentSliderValue;
        public GameObject StartTime;
        public GameObject EndTime;

        void Awake() {
            StartCoroutine(Recenter());
            // _screenPosInZ = new[] { -0.4f, 2, 3.5f };
            _isResChanged = false;
            CurrentScreenPosIndex = 0;
            ZoomIn.SetActive(true);
           ZoomOutGreyedOut.SetActive(true);
           ZoomOut.SetActive(false);
            _isGazeOver = false;
            SceneManager.UnloadScene(Constants.ChannelScene);
            LoadingIcon.SetActive(true);
            LoadingText.SetActive(true);
            MediaCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName = string.Empty;
        }

        private IEnumerator Recenter() {
            yield return new WaitForSeconds(2);
            GvrViewer.Instance.Recenter();
        }

        IEnumerator Start() {
            StartCoroutine(ShowLoadingIcon());
            GvrViewer.Instance.EnableAlignmentMarker = false;
            GvrViewer.Instance.Recenter();
            yield return new WaitForSeconds(1);
            var videoFile = GetVideoFileName();

            MediaCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName = videoFile;
            MediaCtrl.Play();
            _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
        }

        private IEnumerator ShowLoadingIcon() {
            while (string.IsNullOrEmpty(MediaCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName)) {
                yield return null;
            }
           LoadingIcon.SetActive(false);
           LoadingText.SetActive(false);
        }


        private string GetVideoFileName()
        {
            if (HomeInitializer.ChannelClicked.Equals(Constants.SdCardChannelClicked)) 
                return GetSdCardVideoFileName();
            else         
                return GetYoutubeVideoFileName();

        }

        private string GetSdCardVideoFileName() {
          //  LoadingText.SetActive(true);
          //  LoadingText.GetComponent<TextMeshProUGUI>().text = "file:///" + SdCardChannelInitializer.VideoId;
            return "file:///" + SdCardChannelInitializer.VideoId;
        }

        private string GetYoutubeVideoFileName() {
            if (Application.internetReachability == NetworkReachability.NotReachable) {
                return null;
            }
            var youtubeVideo = new YoutubeVideo();
            var url = youtubeVideo.RequestVideo(ChannelInitializer.VideoId, _currentResolution);
            return url;
        }

        void Update() {
            if (string.IsNullOrEmpty(MediaCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName))
                return;
            //if (Math.Abs(Slider.value - Slider.maxValue) < 0.001) {
            //    StartCoroutine(FadeToMenu());
            //    return;
            //}

            if (MediaCtrl.GetComponent<MediaPlayerCtrl>().GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.END) {
                StartCoroutine(FadeToMenu());
                return;
            }

            if (MediaCtrl.GetDuration() != 0) {
                Slider.maxValue = (MediaCtrl.GetDuration() / 1000f);
                var totalTime = TimeSpan.FromSeconds(Slider.maxValue);
                EndTime.GetComponent<TextMeshProUGUI>().text = string.Format("{0}:{1:00}", (int)totalTime.TotalMinutes, totalTime.Seconds);
                //  LoadingIcon.SetActive(false);
                // var totalTime = TimeSpan.FromSeconds(Slider.maxValue);
                //  GameObject.Find("EndTime").GetComponent<Text>().text = string.Format("{0}:{1:00}", (int)totalTime.TotalMinutes, totalTime.Seconds);
            }
            Slider.value = MediaCtrl.GetSeekPosition() / 1000f;
            var time = TimeSpan.FromSeconds(MediaCtrl.GetSeekPosition() / 1000f);
             StartTime.GetComponent<TextMeshProUGUI>().text = string.Format("{0}:{1:00}", (int)time.TotalMinutes, time.Seconds);
            // var time = TimeSpan.FromSeconds(MediaPlayerCtrl.GetSeekPosition() / 1000f);
            //  GameObject.Find("StartTime").GetComponent<Text>().text = string.Format("{0}:{1:00}", (int)time.TotalMinutes, time.Seconds);

            ZoomIn.SetActive(CurrentScreenPosIndex < (_screenPosInZ.Length-1));
            ZoomInGreyedOut.SetActive(!ZoomIn.activeInHierarchy);
            ZoomOut.SetActive(CurrentScreenPosIndex > 0);
            ZoomOutGreyedOut.SetActive(!ZoomOut.activeInHierarchy);
        }

        private IEnumerator FadeToMenu() {
            // Wait for the screen to fade out.
            yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
            // Load the main menu by itself.
            if(HomeInitializer.ChannelClicked.Equals(Constants.YoutubeChannelClicked))
             SceneManager.LoadScene(Constants.ChannelScene, LoadSceneMode.Single);
            else {
                SceneManager.LoadScene(Constants.ChannelSdCardScene, LoadSceneMode.Single);
            }
        }

        public void ChangeResolution(int objectResolution) {
            _currentResolution = objectResolution;
            StartCoroutine(ChangeResolution());
        }

        public void SetResolution(int objectResolution) {
            _currentResolution = objectResolution;
        }

        private IEnumerator ChangeResolution() {
            _isResChanged = true;
            _currentSliderValue = Slider.value;
          //  StartCoroutine(ShowLoadingIcon());
           // yield return new WaitForSeconds(1);
            yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
            var videoFile = GetVideoFileName();
            //  MediaCtrl.GetComponent<MediaPlayerCtrl>().m_strFileName = videoFile;
            MediaCtrl.Load(videoFile);
            MediaCtrl.Play();
            yield return StartCoroutine(_vrCameraFade.BeginFadeIn(true));
            // yield return new WaitForSeconds(1);
            MediaCtrl.SeekTo((int)_currentSliderValue * 1000);

        }
    }
}
