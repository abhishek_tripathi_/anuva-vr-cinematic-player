﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts {
    public class HomeBtn : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        private VRCameraFade _vrCameraFade;
        public GameObject ReticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        private bool _isGazeOver;

        void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            _reticleOrigSize = new Vector3(0.5f,0.5f,0.5f);
            _reticleOverSize = new Vector3(1,1,1);
            _reticleTransform = ReticleGameObject.GetComponent<RectTransform>();
            _isGazeOver = false;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionCompleteHome += HandleSelectionComplete;

            //_vrCameraFade.OnFadeCompleteHome += HandleFadeCompleteHome;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionCompleteHome -= HandleSelectionComplete;
          // _vrCameraFade.OnFadeCompleteHome -= HandleFadeCompleteHome;
            //  _vrCameraFade.OnFadeComplete -= HandleFadeComplete;
        }

        private void HandleOver() {
            _isGazeOver = true;
            _selectionRadial.Show();
            _selectionRadial.HandleDown();
            _selectionRadial.isHomeClicked = true;
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            _isGazeOver = false;
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _selectionRadial.isHomeClicked = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private IEnumerator FadeToMenu() {
            _selectionRadial.isHomeClicked = false;
            // Wait for the screen to fade out.
            yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
            //AsyncOperation o = Application.LoadLevelAsync(Constants.ChannelScene);
            //o.allowSceneActivation = false;
            //while (o.isDone) {
            //    yield return new WaitForEndOfFrame();
            //}
            //o.allowSceneActivation = true;
            // Load the main menu by itself.
            SceneManager.LoadScene(Constants.HomeScene, LoadSceneMode.Single);
        }

        private void HandleSelectionComplete() {
            if (!_isGazeOver)
                return;
            StartCoroutine(FadeToMenu());
        }

        private void HandleFadeCompleteHome() {
            if (_selectionRadial.isHomeClicked) {
                _selectionRadial.isHomeClicked = false;
              //  SceneManager.UnloadScene(Constants.ChannelScene);
                SceneManager.LoadScene(Constants.HomeScene, LoadSceneMode.Single);
            }
        }
    }
}
