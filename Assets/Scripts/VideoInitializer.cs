﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Assets.Scripts.channel;
using Assets.Scripts.playlist;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class VideoInitializer : MonoBehaviour {
        private Vector3[] _prefabPos = {

           new Vector3(-436, 305, -0),               ///// Image 1
            new Vector3(-70,305, -0),                         ///// Image 2
            new Vector3(295,305, -0),
            new Vector3(664,305, -0),           ///// Image 3

            new Vector3(-436, 72, -0),               ///// Image 1
            new Vector3(-70,72, -0),                         ///// Image 2
            new Vector3(295,72, -0),
            new Vector3(664,72, -0),           ///// Image 3


            new Vector3(-436, -162, -0),               ///// Image 1
            new Vector3(-70,-162, -0),                         ///// Image 2
            new Vector3(295,-162, -0),
            new Vector3(664,-162, -0),           ///// Image 3


           new Vector3(-436, -397, -0),               ///// Image 1
            new Vector3(-70,-397, -0),                         ///// Image 2
            new Vector3(295,-397, -0),
            new Vector3(664,-397, -0),

        };

        private Vector3[] _prefabRot = {

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),

            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0),
        };
        private int _gridCount = 16;
        private string YourAPIKey = "AIzaSyDD-lxGLHsBIFPFPt2i31fc0tAHGeAb8mc";
        private int maxresults = 50;
        GameObject parentObj = null;
        private string nextPageToken;
        private bool isFirstPage = true;

        public GameObject Prefab;
        public GameObject ParentPrefab;
        private bool isChannelSelected;
        static private int currentCount = 0;
        private int _parentCount;
        private float cameraDistanceToMove = 46.9f;
        public GameObject VideoPanelParentObj;
        public GameObject VideoInfoParentObject;
        public static string PlaylistId;
        public static bool isMyVideosPlaylist ;
        private List<string> videoIdsList;
        public GameObject MoveObject;
        private int batchLimit;
        private bool isTimeOver;
        private int totalCoroutineRunning;
        private int? totalVideos;


        //void Start() {
        //    VideoPanelParentObj = GameObject.Find("Video");
        //    isFirstPage = true;
        //    nextPageToken = null;
        //    isChannelSelected = false;
        //    currentCount = 0;
        //    _parentCount = 1;
        //    videoIdsList = new List<string>();
        //}

        void OnEnable() {
            totalCoroutineRunning = 0;
            VideoPanelParentObj = GameObject.Find("Video");
            totalVideos =null;
            batchLimit = 30;
            isFirstPage = true;
            nextPageToken = null;
            isChannelSelected = false;
            currentCount = 0;
            _parentCount = 1;
            videoIdsList = new List<string>();
            isTimeOver = true;
            // MoveObject.transform.position = new Vector3(0, 0, MoveObject.transform.position.z);
        }

        void Update() {

            if ( totalVideos!=null && currentCount >= totalVideos) {
                PlaylistInfoInitializer.totalVideos = totalVideos.ToString();
                ChannelInitializer.VideolistMaxValue = (_parentCount - 2) * cameraDistanceToMove;
                return;
            }

            if (ChannelInitializer.IsBackPressed)
                return;
          
            if ((CheckNextTokenForNull(nextPageToken) || isFirstPage )&& currentCount<batchLimit) {
                isFirstPage = false;
                YoutubeV3Call("video");
                
            }
            else {
               
                if (totalCoroutineRunning <= 0)
                    batchLimit = batchLimit + 30;

                //if (isTimeOver )
                //       StartCoroutine(WaitForSomeTimeToLoad());
            }
        }

        public IEnumerator WaitForSomeTimeToLoad() {
            isTimeOver = false;
            yield return new WaitForSeconds(4);
            batchLimit = batchLimit + 35;
            isTimeOver = true;
        }

        public void YoutubeV3Call(string searchType) {
           
            if (nextPageToken == null) {
                if (isMyVideosPlaylist) {
                    StartCoroutine(VideoSearch("https://www.googleapis.com/youtube/v3/search?&key=" + YourAPIKey + "&part=snippet&channelId=" + PlaylistId + "&maxResults=" + maxresults + ""));
                    return;
                }
                StartCoroutine(VideoSearch("https://www.googleapis.com/youtube/v3/playlistItems?&key=" + YourAPIKey + "&part=snippet&playlistId=" + PlaylistId + "&maxResults=" + maxresults + ""));
            }
            else if (nextPageToken != null) {
                if (isMyVideosPlaylist) {
                    StartCoroutine(VideoSearch("https://www.googleapis.com/youtube/v3/search?&key=" + YourAPIKey + "&part=snippet&channelId=" + PlaylistId + "&pageToken=" + nextPageToken + ""));
                    return;
                }
                StartCoroutine(VideoSearch("https://www.googleapis.com/youtube/v3/playlistItems?&key=" + YourAPIKey + "&part=snippet&playlistId=" + PlaylistId + "&pageToken=" + nextPageToken + ""));
            }
        }

        private IEnumerator VideoSearch(string url) {

            totalCoroutineRunning += 1;
            var call = new WWW(url);
            yield return call;
            var youtubeReturn = JSON.Parse(call.text);
            
            nextPageToken = youtubeReturn["nextPageToken"];
            
             totalVideos = !isMyVideosPlaylist ? youtubeReturn["pageInfo"]["totalResults"].AsInt : youtubeReturn["pageInfo"]["totalResults"].AsInt -1;
            youtubeReturn = youtubeReturn["items"];
            for (int i = 0; i < youtubeReturn.Count; i++) {
                //if (((currentCount + 1) % _gridCount) == 1)
                //    parentObj = InstantiateParentPrefab();
                if (!videoIdsList.Contains(youtubeReturn[i]["id"])) {
                    if (isMyVideosPlaylist)
                        if (youtubeReturn[i]["id"]["kind"].Value.Equals("youtube#channel") || youtubeReturn[i]["id"]["kind"].Value.Equals("youtube#playlist"))
                            continue;
                    if (youtubeReturn[i]["snippet"]["title"].Equals("Private video") || youtubeReturn[i]["snippet"]["thumbnails"] == null )
                        continue;
                    if (((currentCount + 1) % _gridCount) == 1)
                        parentObj = InstantiateParentPrefab();
                    if(isMyVideosPlaylist)
                        videoIdsList.Add(youtubeReturn[i]["id"]["videoId"]);
                    else
                    videoIdsList.Add(youtubeReturn[i]["id"]);
                    //yield return null;
                    if (isMyVideosPlaylist) {
                        StartCoroutine(GetVideoInfo(youtubeReturn[i]["id"]["videoId"],
                            youtubeReturn[i]["snippet"]["thumbnails"]["medium"]["url"],
                            youtubeReturn[i]["snippet"]["title"], youtubeReturn[i]["snippet"]["description"],
                            currentCount, parentObj));
                    }
                    else
                        StartCoroutine(GetVideoInfo(youtubeReturn[i]["snippet"]["resourceId"]["videoId"],
                            youtubeReturn[i]["snippet"]["thumbnails"]["medium"]["url"],
                            youtubeReturn[i]["snippet"]["title"], youtubeReturn[i]["snippet"]["description"],
                            currentCount, parentObj));
                    currentCount++;
                }
            }
            totalCoroutineRunning -= 1;
        }

        private IEnumerator GetVideoInfo(string videoId,  string videoThumb, string videoTitle, string desc, int count, GameObject ParentObject) {
            totalCoroutineRunning += 1;
            WWW ncal = new WWW("https://www.googleapis.com/youtube/v3/videos?key=" + YourAPIKey + "&part=contentDetails,statistics&id=" + videoId);
            yield return ncal;
            JSONNode youtubeNewReturn = JSON.Parse(ncal.text);
            JSONNode data = JSON.Parse(youtubeNewReturn["items"][0].ToString());//Fixed, is getting some error to get Data

            string title = videoTitle;
            //if (title.Length > 15) {
            //    title = title.Remove(15);
            //    title = title + "...";
            //}
            string duration = data["contentDetails"]["duration"];
            bool is360Video = (data["contentDetails"]["projection"].Value).Equals("360"); 
            duration = duration.Replace("PT", "");
            duration = duration.Replace("M", ":");
            duration = duration.Replace("S", "");
            //string[] durationStrArr = duration.Split(':');
            //if (Convert.ToInt16(durationStrArr[1]) < 10)
            //{
            //    duration = duration.Remove(duration.Length - 1, 1) + durationStrArr[1].PadLeft(2, '0');
            //}
            string views = data["statistics"]["viewCount"];
            string likes = data["statistics"]["likeCount"];
            string dislikes = data["statistics"]["dislikeCount"];
            var imageObj = Instantiate(Prefab);
            imageObj.transform.parent = ParentObject.transform;
            var imagePos = _prefabPos[(count % _gridCount)];
            var imageRot = _prefabRot[(count % _gridCount)];
            //imageObj.transform.localPosition = imagePos;
            imageObj.GetComponent<RectTransform>().transform.localPosition = imagePos;
            imageObj.GetComponent<RectTransform>().transform.localScale = new Vector3(1, 1, 1);
            imageObj.GetComponent<video.VideoSelector>().VideoId = videoId;
            if (!string.IsNullOrEmpty(desc))
                desc = Regex.Replace(desc, @"[^\u0000-\u007F]", string.Empty);
            imageObj.GetComponent<video.VideoSelector>().Desc = desc;
            if (!string.IsNullOrEmpty(title))
                title = Regex.Replace(title, @"[^\u0000-\u007F]", string.Empty);
            imageObj.GetComponent<video.VideoSelector>().Title = title;
            imageObj.GetComponent<video.VideoSelector>().OrigTitle = videoTitle;
            imageObj.GetComponent<video.VideoSelector>().Dislikes = dislikes;
            imageObj.GetComponent<video.VideoSelector>().Likes = likes;
            imageObj.GetComponent<video.VideoSelector>().Duration = duration;
            imageObj.GetComponent<video.VideoSelector>().Views = views;
            imageObj.GetComponent<video.VideoSelector>().Is360Video = is360Video;
            yield return null;
            //  imageObj.transform.localEulerAngles = imageRot;
            StartCoroutine(DownloadThumbs(videoThumb, imageObj));
            totalCoroutineRunning -= 1;
        }

        private IEnumerator DownloadThumbs(string url, GameObject imageGameObject) {
            totalCoroutineRunning += 1;
            var www = new WWW(url);
            yield return www;
            var texture = new Texture2D(100, 100);
            www.LoadImageIntoTexture(texture);
            var rec = new Rect(0, 0, texture.width, texture.height);
            imageGameObject.GetComponent<Image>().sprite = Sprite.Create(texture, rec, new Vector2(0, 0), 1);
            imageGameObject.GetComponent<video.VideoSelector>().Image = imageGameObject.GetComponent<Image>().sprite;
            totalCoroutineRunning -= 1;
            //  imageGameObject.GetComponentInChildren<CursorSelector>().Texture = texture;
            // imageGameObject.GetComponentInChildren<MeshRenderer>().material.mainTexture = texture;
        }

        private GameObject InstantiateParentPrefab() {
            var parentObject = Instantiate(ParentPrefab);
            parentObject.transform.position = Vector3.zero;
            PlacePrefabInWorld(parentObject);
            return parentObject;
        }

        private void PlacePrefabInWorld(GameObject parentObject) {
            var imagePos = parentObject.transform.position;
            var posX = 0f;
            //if (_parentCount % 2 == 0) { // if even
            //    posX = (_parentCount / 2) * cameraDistanceToMove;
            //}
            //else { // if odd
            //    posX = -(((_parentCount - 1) / 2) * cameraDistanceToMove);
            //}

            posX = (_parentCount - 1)*cameraDistanceToMove;
            parentObject.transform.parent = VideoPanelParentObj.transform;
            //parentObject.transform.localPosition = new Vector3(posX, imagePos.y, imagePos.z);
            parentObject.GetComponent<RectTransform>().transform.localPosition = new Vector3(posX, 0, 16.1f);
            _parentCount++;
        }

        private bool CheckNextTokenForNull(string tokenvalue) {
            return !string.IsNullOrEmpty(tokenvalue);
        }
    }
}
