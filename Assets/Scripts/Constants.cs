﻿
namespace Assets.Scripts {
    public class Constants  {

        // Scene constants
        public static string ChannelScene = "Channel";
        public static string VrVideoScene = "vr_video";
        public static string NonVrScene = "nonvr_video";
        public static string NonVrLoading = "nonvr_video_loading";
        public static string SplashScene = "Splash";
        public static string LandingScene = "LandingScene";
        public static string ChannelSdCardScene = "Channel_SdCard";
        public static string HomeScene = "Home";
        public static string SdCardDefaultRootScene = "Channel_SdCardDefaultRoot";

        // VideoSource Constants
        public static string YoutubeChannelClicked = "Youtube";
        public static string SdCardChannelClicked = "SdCard";
        public static string FilmOnTvChannelClicked = "FilmOnTv";

        // SdCardDefaultRootPath
        public static string SdCardDefaultRootPath = "SdCardDefaultRootPath";

    }
}
