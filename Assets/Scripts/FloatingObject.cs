﻿using UnityEngine;
using System.Collections;

public class FloatingObject : MonoBehaviour {

	public float floatDistance = 0.4f;
	bool floatup;
	
	// Use this for initialization
	void Start () {
		floatup = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if(floatup)
            StartCoroutine(FloatingUp());
		else if(!floatup)
            StartCoroutine(FloatingDown());
	}

    IEnumerator FloatingUp()
	{
	     this.transform.position = new Vector3(transform.position.x, (transform.position.y + floatDistance * Time.deltaTime), transform.position.z);
	     yield return new WaitForSeconds(1);
	     floatup = false;
	 }

    IEnumerator FloatingDown()
	 {
        this.transform.position = new Vector3(transform.position.x, (transform.position.y - floatDistance * Time.deltaTime), transform.position.z);
        yield return new WaitForSeconds(1);
	     floatup = true;
	 }
}
