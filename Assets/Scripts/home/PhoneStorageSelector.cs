﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace Assets.Scripts.home {
    public class PhoneStorageSelector : MonoBehaviour {

        public VRInteractiveItem VrItem;
        private SelectionRadial _selectionRadial;
        private bool _gazeOver;
    //    private VRCameraFade _vrCameraFade;
        public Animation SourceSelector;
        public Animation SourceDeselector;

        private void Awake() {
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
         //   _vrCameraFade = GameObject.Find("CardboardMain").GetComponent<VRCameraFade>();
            _gazeOver = false;
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
          //  _vrCameraFade.OnFadeComplete += HandleFadeComplete;
        }

        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
         //   _vrCameraFade.OnFadeComplete -= HandleFadeComplete;
        }

        private void HandleOver() {
            gameObject.GetComponents<AudioSource>()[0].Play();
            SourceSelector.Stop("SourceDeselector");
            SourceSelector.Play("SourceSelector");
            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
        }

        private void HandleOut() {
            SourceSelector.Stop("SourceSelector");
            SourceSelector.Play("SourceDeselector");
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
        }

        private void HandleSelectionComplete() {
            if (!_gazeOver)
                return;

            gameObject.GetComponents<AudioSource>()[1].Play();
            HandleOut();
            HomeInitializer.ChannelClicked = Constants.SdCardChannelClicked;
            SceneManager.LoadScene(Constants.ChannelSdCardScene, LoadSceneMode.Single);
           // StartCoroutine(FadeToMenu());
        }

        //private IEnumerator FadeToMenu() {
        //    yield return StartCoroutine(_vrCameraFade.BeginFadeOut(true));
        //}

        //private  void HandleFadeComplete() {
        //    SceneManager.LoadScene(Constants.ChannelSdCardScene, LoadSceneMode.Single);
        //}

    }
}
