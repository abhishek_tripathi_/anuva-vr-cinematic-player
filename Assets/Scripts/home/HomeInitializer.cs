﻿using System;
using UnityEngine;

namespace Assets.Scripts.home {
    public class HomeInitializer : MonoBehaviour {

        public static string ChannelClicked;

        void OnEnable() {
            ChannelClicked = string.Empty;
        }
    }

}
