﻿using UnityEngine;

namespace Assets.Scripts {
    public class LookDownNonVr : MonoBehaviour {


        public GameObject Head;
        //  public GameObject HidingPlane;
        public GameObject Reticle;

        ////// params
        public int FirstAngleInX;
        public int SecondAngleInX;

        void Update() {
            UpdateRotation();
        }

        private void UpdateRotation() {
            if (Head.transform.localRotation.eulerAngles.x > FirstAngleInX && Head.transform.localRotation.eulerAngles.x < SecondAngleInX) {
               Reticle.SetActive(true);
                return;
            }
            Reticle.SetActive(false);
        }
    }
}
