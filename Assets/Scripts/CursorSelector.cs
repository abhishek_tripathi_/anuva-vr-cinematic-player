﻿using Assets.Scripts.channel;
using Assets.Scripts.playlist;
using TMPro;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts {
    public class CursorSelector : MonoBehaviour {

        public VRInteractiveItem VrItem;
      //  private SelectionRadial _selectionRadial;
       // private VRCameraFade _vrCameraFade;
    //    [HideInInspector]public int AssetIndex;
    //    private bool _gazeOver;
        private Vector3 _originalScale;
        private Vector3 _hoverScale;
      //  private Vector3 _reticleOrigSize;
     //   private Vector3 _reticleOverSize;
     //   private Transform _reticleTransform;
        //[HideInInspector]
        public string PlaylistId;
        [HideInInspector]
        public string Desc;
        [HideInInspector]
        public string Title;
        [HideInInspector]
        public string TotalVideos;
        [HideInInspector] public Sprite Sprite;
        public GameObject[] ToggleVisibleObjects;
        public GameObject VideoPanelParentObject;
        public bool isToggleObjectsSelected;
        private SelectionRadial _selectionRadial;
        public bool _gazeOver;
        private GameObject _reticleGameObject;
        private Vector3 _reticleOrigSize;
        private Vector3 _reticleOverSize;
        private Transform _reticleTransform;
        public GameObject VideoGameObject;
        public GameObject PlaylistInfoParentObject;
        public GameObject PlaylistBG;
       void Awake() {
            VideoGameObject = GameObject.Find("Video");
           //VideoGameObject = VideoPanelParentObject.GetComponent<VideoInitializer>().VideoPanelParentObj;
            isToggleObjectsSelected = false;
            _selectionRadial = GameObject.Find("CardboardMain").GetComponent<SelectionRadial>();
            _reticleGameObject = GameObject.Find("Background");
            _gazeOver = false;
            _reticleOrigSize = new Vector3(0.5f, 0.5f, 0.5f);
            _reticleOverSize = new Vector3(0.8f, 0.8f, 0.8f);
            _reticleTransform = _reticleGameObject.GetComponent<RectTransform>();
        }

        private void OnEnable() {
            VrItem.OnOver += HandleOver;
            VrItem.OnOut += HandleOut;
            _selectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }


        private void OnDisable() {
            VrItem.OnOver -= HandleOver;
            VrItem.OnOut -= HandleOut;
            _selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }

        private void HandleOver() {
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(true);
            }
            PlaylistBG.SetActive(true);
            //TitleObject.SetActive(true);
            //TitleObject.GetComponentInChildren<TextMeshProUGUI>().text = Title;

            _selectionRadial.Show();
            _gazeOver = true;
            _selectionRadial.HandleDown();
            _reticleTransform.localScale = _reticleOverSize;
        }

        private void HandleOut() {
            //if (isToggleObjectsSelected)
            //    return;
            if((ChannelInitializer.CurrentActivePlaylist == null )|| !ChannelInitializer.CurrentActivePlaylist.Equals(gameObject))
            PlaylistBG.SetActive(false);
            foreach (var toggleObj in ToggleVisibleObjects) {
                toggleObj.SetActive(false);
            }
            //TitleObject.SetActive(false);
            _selectionRadial.Hide();
            _selectionRadial.HandleUp();
            _gazeOver = false;
            _reticleTransform.localScale = _reticleOrigSize;
        }

        private void HandleSelectionComplete() {
             if (!_gazeOver)
                return;
            
            if (ChannelInitializer.CurrentActivePlaylist != null) {
                ChannelInitializer.CurrentActivePlaylist.GetComponent<Animator>().Rebind();
                ChannelInitializer.CurrentActivePlaylist.GetComponent<Animator>().enabled = false;
                ChannelInitializer.CurrentActivePlaylist.GetComponent<CursorSelector>().PlaylistBG.SetActive(false);
                var currPos = ChannelInitializer.CurrentActivePlaylist.GetComponent<RectTransform>().localPosition;
                ChannelInitializer.CurrentActivePlaylist.GetComponent<RectTransform>().localPosition = new Vector3(currPos.x, currPos.y, 0);
            }
            ChannelInitializer.CurrentActivePlaylist = gameObject;
            HandleOut();
            gameObject.GetComponent<Animator>().enabled = true;
           //PlaylistBG.SetActive(true);
          
            //VideoPanelParentObject.SetActive(false);
            // VideoPanelParentObject.SetActive(true);
            foreach (Transform child in VideoGameObject.transform) {
                Destroy(child.gameObject);
            }
            VideoPanelParentObject.SetActive(false);
            PlaylistInfoInitializer.desc = Desc;
            PlaylistInfoInitializer.title = Title;
            PlaylistInfoInitializer.PlaylistId = PlaylistId;
            PlaylistInfoInitializer.PlaylistImage = Sprite;
            PlaylistInfoInitializer.isFirst = true;
            VideoPanelParentObject.SetActive(true);
            VideoInitializer.isMyVideosPlaylist = false;

            // VideoPanelParentObject.GetComponent<PlaylistInitializer>().OnEnable();
            VideoInitializer.PlaylistId = PlaylistId;
            ChannelInitializer.IsBackPressed = false;

          //  PlaylistInfoParentObject.SetActive(false);
          //  PlaylistInfoParentObject.SetActive(true);
            
        }

    }
}
